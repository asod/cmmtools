from cmm.md.simple_systems import LennardJonesFluidSigma, unit

'''
    This creates a 5 100 Å (!) boxes and changes the sigma of the first
    particle in each box, before saving prmtops and inpcrds.

    The density is close to the one found here:
    http://www.sklogwiki.org/SklogWiki/index.php/Argon

    Note that the reduced density is calculated based on the
    density of the rest of the particles, using the standard sigma of
    3.4 Å.

'''

nparticles = 20713
sigma = 3.4

box_edge = 100
volume = box_edge**3
reduced_density = sigma**3 * nparticles / volume
sigma *= unit.angstrom

for mod_sigma in [2, 3.4, 4, 5, 10]:

    sigma_list = [sigma for x in range(nparticles)]
    sigma_list[0] = mod_sigma * unit.angstrom

    fluid = LennardJonesFluidSigma(nparticles=nparticles, sigma=sigma,
                                   sigma_list=sigma_list,
                                   reduced_density=reduced_density)

    fluid.save(f'box100_sig{mod_sigma:05.2f}.prmtop', overwrite=True)
    fluid.save(f'box100_sig{mod_sigma:05.2f}.inpcrd', overwrite=True)