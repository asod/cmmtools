%Chk=FECN6_small_opt.chk
%Mem=3000MB
%NProcShared=2
# B3LYP/6-31G* Geom=PrintInputOrient Integral=(Grid=UltraFine) Opt
 
CLR
 
-4  1
C           8.113   10.098    8.113
N           8.113   11.307    8.113
C           6.129    8.113    8.113
N           4.920    8.113    8.113
C           8.113    6.129    8.113
N           8.113    4.920    8.113
C           8.113    8.113    6.129
N           8.113    8.113    4.920
C          10.098    8.113    8.113
N          11.307    8.113    8.113
C           8.113    8.113   10.098
N           8.113    8.113   11.307
Fe          8.113    8.113    8.113
 
 
