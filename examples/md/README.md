Examples of using CMMSystem to do MD
====================================

The more advanced ones are basically runscripts, so not very pedagogical, but performing a lot of tasks :) 


`example.py`
-----------
Simplest possible run. 


`agptpop_es_constrainedopt_conandres_noNPT.py`
----------------------------------------------
Running solvent sampling simulations for all ES ChelpG charge sets made by scanning the vdw radii of Ag and Pt in AgPtPOP.  

The ES structure is made by updating the original positions in the system with an xyz file containing positions from the first
run of x-ray structural fitting of the metals, and DFT-relaxed ligands. 


`meptpop_kruppa.py`
-------------------
Sample shells of all Kruppa structures. Look in here for an idea for how to reindex molecules based on closest distance to atoms
of the same element. Tests of this are also visualized in `/data/md/tlptpop/TlPtPOP_shells.ipynb`


`AtomicIon_Water.ipynb`
-----------------------
Notebook showing how to quickly and simply create I- in water, as well as how to run simulations of I0, basically simply 
removing the charge on I-. 


`FeTPY_MCPB.ipynb - Parametrization of TM complexes within AMBER-type FFs`
--------------------------------------------------------------------------
Notebook walked-through example of using the MCPB.py to fully parametrize a transition metal complex, using Gaussian DFT
frequency calculations to get internal bond constants through the Seminario method, as well as getting partial charges
from RESP calculations. The complex used for the example is Fe(tpy)2 


`dmf_opls_custombox.ipynb: Hacky way to make a custom solvent box in OpenMM`
------------------------------------------------------------------------------------
Starts from a single solvent molecule as a .pdb, with the correct CONECT terms (openbabel can get you those (or at least some)),
uses `packmol` within `openmoltools` to span a box with a complete topology, abuses the OpenMM `GromacsTopFile` to make an OpenMM
system. 

