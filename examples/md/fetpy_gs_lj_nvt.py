from os.path import isdir
import numpy as np
from cmm.md.cmmsystems import CMMSystem, angstroms
from cmm.tools.parsers import read_chargefile
from parmed.openmm.reporters import RestartReporter

'''
    FeTPY with Fitted LJ Parameters.

    Read last step of previous NVT
    Change LJ Params
    Set Constraints
    Run

    On HPC: 
       voltash
       source ~/.openmm
    '''


tag = 'fetpy_gs_ljfit_nvt' 

fpath = f'../../data/FeTPY/{tag}'


sys = CMMSystem(tag, fpath=fpath)

## and following NVT for longer
sys = CMMSystem(tag, fpath=fpath)

sys.system_from_prmtop('../../data/md/fetpy/FETPY_solv.prmtop')


sys.load_rst7('/zhome/c7/a/69784/openMM/data/FeTPY/fetpy_gs_pushaway_onlyfeconstrain/fetpy_gs_pushaway_onlyfeconstrain_nvt.rst7')

# Get indices
ns_idx, syms = sys.get_nonsolvent_indices(tag='Cl-')
ct_idx = [i for i, sym in enumerate(syms) if sym == 'Cl-']
solu_idx = [i for i, sym in enumerate(syms) if sym != 'Cl-']

# set new LJ
#FIT
from simtk.unit import kilocalorie_per_mole, angstroms
# dict with all uppercase keys, element:(epsilon, sigma)
fit_lj = {'C': (0.03799998 * kilocalorie_per_mole, 3.76945 * angstroms),
          'N': (0.16317734 * kilocalorie_per_mole, 1.88924 * angstroms),
          'FE': (0.00649996 * kilocalorie_per_mole, 1.29700 * angstroms),
          'H': (0.00000000 * kilocalorie_per_mole, 1.50000 * angstroms)}

fit_manual = {'C': (0.22799988 * kilocalorie_per_mole, 3.43020 * angstroms),
              'N': (0.16317734 * kilocalorie_per_mole, 1.88924 * angstroms),
              'Fe': (0.00649996 * kilocalorie_per_mole, 1.29700 * angstroms),
              'H': (0.00000000 * kilocalorie_per_mole, 1.50000 * angstroms)}


#chrgs, sigs, epss = sys.get_nonbonded(solu_idx)
sys.adjust_lj(solu_idx, fit_lj)

# restrain 
sys.restrain_atoms([58] + ct_idx, 500)

# and integrator
sys.set_integrator(timestep=2.0)  

# init sim
sys.init_simulation(platform='CUDA', double=False)

# reporters
sys.standard_reporters()

# restart reporter
restrt = RestartReporter(fpath + f'{tag}.rst7', 1000) 
sys.add_reporter(restrt)

# run
sys.run(time_in_ps=20000)
