'''
How to get custom CM coefficients for Debye scattering.
'''

import matplotlib.pyplot as plt
import numpy as np
from ase import Atoms
from cmm.xray.debye import Debye

atoms = Atoms('I2', positions=np.array([[0, 0, -1], [0, 0, 1]]))
elements = atoms.get_chemical_symbols()
elements[-1] = 'I0'  # make 2nd I I0
deb = Debye()
s_I0 = deb.debye(atoms, custom_elements=elements)
s_Im = deb.debye(atoms)

fig, ax = plt.subplots(figsize=(9,6))
ax.plot(deb.qvec, s_I0, label='One atom is I0')
ax.plot(deb.qvec, s_Im, label='Both atoms I-')

ytext = r'S(Q) (e.u.)'
xtext = 'Q (Å$^{-1}$)'
ax.set_ylabel(ytext)
ax.set_xlabel(xtext)
ax.legend(loc='best')