Examples
========

MD
--

`md/FeTPY_MCPB.ipynb`: Notebook going through GAFF/AMBER parametrization of [Fe(tpy)<sub>2</sub>]<sup>2+</sup> using MCPB.py. Reguires Gaussian (or GAMESS) for partial charge parametrisation. 

`examples.py`: Very simple OpenMM MD run of a complex in water  

`agptpop_gs_bigbox_pushaway_npt.py`: Bit more advanced example, exchanging patial charges and doing other stuff. Mostly for inspiration, paths to files are not set to work directly within this repo. 

Xray
----

Various tests of the WAXS-potential.