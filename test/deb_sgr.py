from cmm.xray.debye import Debye
from cmm.xray.sgr import SGr
from ase.io import read

''' Test that the Debye and RDF scattering gives same results for
    a test solute of two Pt atoms.

    Since the RDF is done with numerical binning (of 0.0001 Ang here)
    there will be a small numerical error '''

atoms = read('../data/test/test.xyz')
deb = Debye()
s_deb = deb.debye(atoms)

stoich = {'Pt_u':2}
V = 10**3
sgr = SGr(V, damp=None, delta=False, verbose=False, qvec=deb.qvec)
sgr.load_rdfs_fromdir('../data/test/')
s_sgr = sgr.calculate(sgr.rdfs, stoich)


assert max((s_deb - s_sgr['s_u']) / s_deb) < 1e-4


