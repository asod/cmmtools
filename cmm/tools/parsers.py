from ase import Atoms, units
from ase.calculators.calculator import Calculator, all_changes
from ase.io import Trajectory
import numpy as np 

def read_chargefile(fname, string):
    '''' Reads charges containing "string" from
         a file made from grepping a bunch of gaussian
         outputs like so:

        grep -A 41 'ESP charges:' *log > charges.dat
        for a 39 atom molecule. Its important that 
        the last line for each charge set is:
        Sum of ESP charges = 

     '''

    with open(fname, 'r') as f:
        lines = f.readlines()

    out = []
    for l, line in enumerate(lines):
        if string in line:
            out.append(line.split(string)[-1])

    out = out[2:-1]

    chrgs = np.array([[int(y[0]), float(y[2])]
                       for y in [x.split()[-3:]
                       for x in out]])

    elmnts = [x.split()[2] for x in out]

    return chrgs, elmnts

def read_orca_chargefile(chgfile):
    ''' Reads charges piped to a file from the results of
        orca_chelpg.
        Works with ORCA version 4.2.0, but is very crude,
        Be careful.
    '''
    with open(chgfile, 'r') as f:
        lines = f.readlines()

    elements = []
    charges = []
    for line in lines[23:]:
        if '--------------------------------' in line:
            break
        charges.append(float(line.split()[-1]))
        elements.append(line.split()[1])

    return charges, elements

''' The methods below all requires you to load the lines of the out first... 
    so 
    >>> with open(some_orca.out, 'r') as f:
    >>>     lines = f.readlines()
    >>> atoms = get_geom(lines, max_atoms=number_of_atoms_in_your_system)
'''

def get_geom(lines, max_atom):
    ''' Create ASE atoms object from an ORCA out
        lines (list): list of orca output lines
        maxatoms (int): number of atoms in your system '''
    traj = []
    for l, line in enumerate(lines):
        tmp = []
        if 'CARTESIAN COORDINATES (ANGSTROEM)' in line:
            for i in range(l + 2, l + 2 + max_atom):
                tmp.append(lines[i].strip())
            symbols = [l.split()[0] for l in tmp]
            pos = np.array([[float(x) for x in l.split()[1:]] for l in tmp])
            atoms = Atoms(symbols, pos)
            traj.append(atoms)
    return traj

def get_energies(lines):
    ''' Get list of energies from, say, a geom opt. A single point returns 
        a list with 1 entry.
        lines (list): list of orca output lines
    '''
    energies = []
    for l, line in enumerate(lines):
        if 'FINAL SINGLE POINT ENERGY' in line:
            energies.append(float(line.split()[-1]) * units.Hartree )
    return energies

def get_forces(lines, max_atom):
    ''' Returns list of atomic forces 
        lines (list): list of orca output lines
        maxatoms (int): number of atoms in your system '''
    traj = []
    for l, line in enumerate(lines):
        tmp = []
        if 'CARTESIAN GRADIENT' in line:
            for i in range(l + 3, l + 3 + max_atom):
                tmp.append(lines[i].strip())
            f = np.array([[float(x) for x in l.split()[3:]] for l in tmp])
            
            traj.append(f * units.Hartree / units.Bohr)
    return traj

def calc_fmax(forces):
    fmax = np.zeros(len(forces))
    for i, f in enumerate(forces):
        fs = np.linalg.norm(f, axis=1)
        fmax[i] = np.max(fs)
    return fmax

def get_occupation(lines):
    ''' NB ONLY FOR UNRESTRICTED CALCS SO FAR '''
    restricted = True
    for l, line in enumerate(lines):
        if 'ORBITAL ENERGIES' in line:
            if 'SPIN' in lines[l + 2]:
                restricted = False
                chnl = lines[l + 2].split()[1].lower()
            orbs = {'up': [], 'down':[]}
            if not restricted:
                for i, orbline in enumerate((lines[l + 4:])):
                    if '************' in lines[i + l + 4 + 1]:
                        break
                    orbs[chnl].append(orbline.strip())
                    if len(orbline.split()) < 1:  # time for other channel
                        del orbs[chnl][-1] # remove the empty line added to end of first channel
                        chnl = lines[i + l + 4 + 1].split()[1].lower()
            fnl_orbs = {'up': [], 'down':[]}
            for i, (chnl, orb) in enumerate(orbs.items()):
                tmp = [line.split() for line in orb]
                fnl_orbs[chnl] = np.array(tmp[i * 2:], dtype=np.float32)

    return fnl_orbs
   

def get_states(lines):
    ''' Gets the states from the orca output block:
        ------------------------------------                                                                                                                                           
        TD-DFT/TDA EXCITED STATES (SINGLETS)                                                                                                                                           
        ------------------------------------  
    '''
    states = {} 
    for i, line in enumerate(lines):
        if 'TD-DFT/TDA EXCITED STATES (SINGLETS)' in line:
            # now were in the right section
            for j, l in enumerate(lines[i + 2:]):
                if '--------------------------' in l:
                    break  # you're done
                if 'STATE' in l:
                    state = int(l.split()[1].split(':')[0])
                    energy = float(l.split()[5])
                    orbs = []
                    weights = []
                    for ol in lines[i + j + 3:]:
                        if 'STATE' in ol:
                            break  
                        parts = ol.split()
                        if len(parts) == 0:
                            break  # this one is done
                        orbs.append([(parts[0]),(parts[2])])
                        weights.append(float(parts[4]))
                        #print(orbs)
                    states[state] = {'energy':energy,
                                     'transitions':orbs,
                                     'weights':weights}
                    
        else:
            continue
    return states


def get_nto_states(lines):
    ''' Gets the states from the orca output blocks:
        ------------------------------------------                                                                                                                                     
        NATURAL TRANSITION ORBITALS FOR STATE XXX                                                                                                                                     
        ------------------------------------------           
    '''
    states = {} 
    for i, line in enumerate(lines):
        if 'NATURAL TRANSITION ORBITALS FOR STATE' in line:
            # now were in the right section
            for j, l in enumerate(lines[i + 8:]):
                if '--------------------------' in l:
                    break  # you're done
                if 'E=' in l:
                    state = int(line.split()[-1])
                    energy = float(l.split()[3])
                    orbs = []
                    weights = []
                    for ol in lines[i + j + 9:]:
                        parts = ol.split()
                        if len(parts) == 0:
                            break  # this one is done
                        orbs.append([(parts[0]),(parts[2])])
                        weights.append(float(parts[5]))
                    states[state] = {'energy':energy,
                                     'transitions':orbs,
                                     'weights':weights}
                    
        else:
            continue

    # add the foscs and wavelengths to each state also. 
    for i, line in enumerate(lines):
        if 'ABSORPTION SPECTRUM VIA TRANSITION ELECTRIC DIPOLE MOMENTS' in line: 
            for l in lines[i + 5:]:
                parts = l.split()
                if len(parts) == 0:
                    break
                state = int(parts[0])
                if state in states:
                    wl = float(parts[2])
                    fosc = float(parts[3])

                    states[state]['fosc'] = fosc
                    states[state]['wl'] = wl



    return states

class DummyCalculator(Calculator):
    ''' Dummy calc for saving energies and forces'''
    implemented_properties = ['energy', 'forces']
    nolabel = True

    def __init__(self, energy=0.0, forces=None, **kwargs):
        Calculator.__init__(self, **kwargs)
        self.external_energy = energy
        self.external_forces = forces if forces is not None else np.zeros((1, 3))

    def calculate(self, atoms=None, properties=['energy'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        self.results['energy'] = self.external_energy
        self.results['forces'] = self.external_forces


def out_to_traj(outfile, max_atoms):
    ''' Create ASE trajectory from an ORCA output file <output>.out
        that contains energies and forces. 
        The output name will be <output>.traj 
    '''

    with open(outfile, 'r') as f:
        lines = f.readlines()

    atoms_list = get_geom(lines, max_atom=110)
    ener = get_energies(lines)
    forc = get_forces(lines, max_atom=110)

    # Create a trajectory file to store the configurations
    traj = Trajectory(outfile.replace('.out', '.traj'), 'w')

    for energy, force, atoms in zip(ener, forc, atoms_list):
        # Attach the calculator with the specific energy and forces
        atoms.calc = DummyCalculator(energy=energy, forces=force)
        atoms.get_potential_energy()  # from external to results... 
        atoms.get_forces()
        
        # Write the configured Atoms object to the trajectory
        traj.write(atoms)

    traj.close()