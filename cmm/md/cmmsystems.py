import os, rmsd
import numpy as np
from sys import stdout
try:
    from simtk.unit import md_unit_system
    from simtk.openmm import app, Platform
    from simtk.openmm.vec3 import Vec3
    from simtk.openmm.openmm import (NonbondedForce, HarmonicBondForce,
                                    HarmonicAngleForce, PeriodicTorsionForce,
                                    MonteCarloBarostat, LangevinIntegrator,
                                    CustomExternalForce, CustomNonbondedForce)

    from simtk.app import GromacsGroFile, GromacsTopFile
    import simtk.unit as u
    from simtk import openmm as mm
except ImportError:
    from openmm.unit import md_unit_system
    from openmm import app, Platform
    from openmm.vec3 import Vec3
    from openmm.openmm import (NonbondedForce, HarmonicBondForce,
                                    HarmonicAngleForce, PeriodicTorsionForce,
                                    MonteCarloBarostat, LangevinIntegrator,
                                    CustomExternalForce, CustomNonbondedForce)

    import openmm.unit as u
    from openmm import openmm as mm
    from openmm.app import GromacsGroFile, GromacsTopFile


from parmed import load_file
from parmed.openmm.reporters import RestartReporter
from openmmtools.integrators import VelocityVerletIntegrator
from openmmtools.testsystems import (TestSystem, DEFAULT_CUTOFF_DISTANCE,
                                     DEFAULT_EWALD_ERROR_TOLERANCE,
                                     DEFAULT_SWITCH_WIDTH)

# WIP: Get this one up and running as well:
#from openmmtools.forces import FlatBottomRestraintForce
import MDAnalysis as MDA
from MDAnalysis.transformations import wrap

class CMMSystem(TestSystem):
    """ Class designed to do all the normal things we do to
        whatever poor small (TM) molecule in solution.

        Remember to set constraints and forces BEFORE
        initializing simulation object.

        Can read .prmtop/inprc
        (or only prmtop and set the positions yourself)

        Can also read .top/.gro

        For example:
        # init
        .. code-block:: python

        >>>sys = CMMSystem(tag='my_simulation',
        >>>                fpath='./agptpop_gs_npt_gpu')
        >>>sys.system_from_prmtop('../../data/md/some_example/some.prmtop',
        >>>                       inpcrd='../../data/md/some_example/some.inpcrd')
        >>> # add your restraints, edit charges, LJ parameters, ...
        >>>sys.add_barostat()
        >>>sys.set_integrator()
        >>>sys.init_simulation()
        >>>sys.minimize()  # if you need that
        >>>sys.mbd()  # Initial Maxwell-Boltzmann vel dist. If needed.
        >>>sys.standard_reporters()
        >>>sys.run(time_in_ps=200)

        Parameters
        -----------
        tag : str,
            prefix of all output files

        WIP . """


    def __init__(self, tag, fpath='./',  constraints=app.HBonds,
                 nonbonded_cutoff=DEFAULT_CUTOFF_DISTANCE,
                 ewald_error_tolerance=DEFAULT_EWALD_ERROR_TOLERANCE,
                 nonbonded_method=app.PME,
                 switch_width=DEFAULT_SWITCH_WIDTH,
                 **kwargs):

        TestSystem.__init__(self, **kwargs)

        self.tag = tag
        self.fpath = fpath
        os.makedirs(fpath, exist_ok=True)

        self.nbc = nonbonded_cutoff
        self.ert = ewald_error_tolerance
        self.nbm = nonbonded_method
        self.sww = switch_width

        self.con = constraints
        self.boxvectors = None
        self.velocities = None
        self.simulation = None


    def system_from_empty_inside(self, ei):
        self.topology = ei.topology
        self.system = ei._system
        self.boxvectors = ei.system.getDefaultPeriodicBoxVectors()

    def system_from_testsystem(self, system):
        ''' Load in already created OpenMMTools system. '''
        self.topology = system.topology
        self.positions = system.positions
        self.system = system.system

    def system_from_gromacs(self, top_file, gro_file):
        ''' Load in gromacs formatted top and gro '''
        gro = GromacsGroFile(gro_file)
        if gro.getPeriodicBoxVectors():
            top = GromacsTopFile(top_file,
                periodicBoxVectors=gro.getPeriodicBoxVectors())
        else:
            top = GromacsTopFile(top_file)

        system = top.createSystem(nonbondedMethod=self.nbm, nonbondedCutoff=self.nbc,
                                  constraints=self.con)
        self.system = system
        self.topology = top.topology
        self.positions = gro.positions

    def system_from_prmtop(self, prmtop_file, inpcrd=None):
        self.prmtop_file = prmtop_file

        prmtop = app.AmberPrmtopFile(prmtop_file)

        system = prmtop.createSystem(nonbondedMethod=self.nbm, nonbondedCutoff=self.nbc,
                                     constraints=self.con)

        self.system = system
        self.topology = prmtop.topology

        if inpcrd:
            inpcrd = app.AmberInpcrdFile(inpcrd)
            positions = inpcrd.getPositions(asNumpy=True)
            self.positions = positions

    def update_positions(self, numpy_ang_array):
        ''' Updates the positions from a numpy (N,3) array
            in angstrom. Does NOT update context, so do BEFORE init_simulation() '''
        pos = numpy_ang_array
        vec3_pos = [[] for x in range(len(pos))]
        for i, p in enumerate(pos):
            vec3_pos[i] = Vec3(*p) * u.angstroms
        self.positions = vec3_pos

    def pos_from_dcd(self, dcd_file, frame=-1, rewrap=False):
        ''' Updates self.positions from dcd_file frame'''
        uni = MDA.Universe(self.prmtop_file)
        uni.load_new(dcd_file)
        if rewrap:  # WIP, not properly tested
            transform = wrap(uni.atoms, compound='residues')
            uni.trajectory.add_transformations(transform)
        frame = uni.trajectory[frame]
        pos = frame.positions
        vec3_pos = [[] for x in range(len(pos))]
        for i, p in enumerate(pos):
            vec3_pos[i] = Vec3(*p) * u.angstroms

        self.positions = vec3_pos

        # boxvectors
        x, y, z = uni.dimensions[:3]
        boxvectors = (Vec3(x, 0, 0) * u.angstrom,
                      Vec3(0, y, 0) * u.angstrom,
                      Vec3(0, 0, z) * u.angstrom)
        self.boxvectors = boxvectors


    def load_rst7(self, rst7_file):
        """ Loads positions, velocities and box vectors from rst7 """

        rst = load_file(rst7_file)
        self.positions = rst.positions

        self.velocities = [vel * u.angstrom / u.picosecond for vel in rst.velocities]

        if rst.box is not None:
            x, y, z = rst.box[:3]
            boxvectors = (Vec3(x, 0, 0) * u.angstrom,
                          Vec3(0, y, 0) * u.angstrom,
                          Vec3(0, 0, z) * u.angstrom)
            self.boxvectors = boxvectors

    def get_nonbonded(self, idx):
        ''' Return current nonbonded params '''
        charges = []
        sigmas = []
        epsilons = []

        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumParticles()):
                    if i not in idx:
                        continue
                    charge, sigma, epsilon = force.getParticleParameters(i)
                    charges.append(charge)
                    sigmas.append(sigma)
                    epsilons.append(epsilon)

        return charges, sigmas, epsilons

    def adjust_masses(self, idx, new_masses):
        ''' Change masses in the idx list to the list of masses in new_masses. '''

        if type(new_masses[0]) != u.Unit:  #  add units, assuming dalton (g/mol)
            new_masses = [nm * u.dalton for nm in new_masses]

        old_m = [self.system.getParticleMass(i) for i in idx]
        for i in idx:
            self.system.setParticleMass(i, new_masses[i])
            print(f'OLD: {old_m[i]._value:6.4f}, NEW: {new_masses[i]._value:6.4f}')

    def lj_opls_to_amber(self, sigma):
        ''' OPLS uses sigma-form LJ potential,
            AMBER uses Rmin. http://docs.openmm.org/latest/userguide/theory.html

            Rmin = 2**(1/6) * sigma '''
        return sigma * 2**(1 / 6)

    def adjust_lj(self, idx, new_lj, opls=False, advanced=False,
                 epsilons=None, sigmas=None):
        ''' Sets new LJ parameters based on the new_lj.

            new_lj should be a dict with keys
            of atom names, and tuples of (eps, sigma) values,
            in kcal/mol and angstrom units, e.g.:

                {'Fe':(0.00649996 * kilocalorie_per_mole, 1.29700 * angstroms)}

            opls: True: converts sigma to Rmin as is used in AMBER.
                  However, from looking at the sigma values for tip4pew
                  in the NonbondedForce object, it seems like openMM
                  ALSO uses sigma values. So most likely, this should
                  not be done.

        '''

        syms = [a.name for i, a in enumerate(self.topology.atoms())
               if i in idx]

        # Fill lists with new parameters from dict
        if epsilons is None:
            epsilons = []
            sigmas = []
            for sym in syms:
                typ = ''.join(s for s in sym if not s.isdigit())
                if advanced:  # put in for every type, not simply symbol
                    eps, sig = new_lj[sym]
                else:
                    eps, sig = new_lj[typ]
                if opls:
                    sig = self.lj_opls_to_amber(sig)
                epsilons.append(eps)
                sigmas.append(sig)

        ct = 0
        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumParticles()):
                    if i not in idx:
                        continue
                    charge, sigma, epsilon = force.getParticleParameters(i)
                    # pylint: disable=maybe-no-member
                    if type(charge) == float:
                        charge *= u.elementary_charge
                    force.setParticleParameters(i, charge,
                                                sigmas[ct].in_units_of(u.nanometer),
                                                epsilons[ct].in_units_of(u.kilojoule_per_mole))
                    print(f'SIG: OLD: {sigma._value:9.6f}, NEW: {sigmas[ct].in_units_of(u.nanometer)._value:9.6f} ' +
                          f'EPS: OLD: {epsilon._value:9.6f}, NEW: {epsilons[ct].in_units_of(u.kilojoule_per_mole)._value:9.6f}')
                    ct += 1

        ### 1-4 SCALING: Only for AMBER - TODO: make it possible to turn it off.
        # The original LJ parameters in these force terms are not scaled, but simply
        # Lorenz-Berthelot-combined. We do the same! Epsilon is LB combined and scaled by 1/2.
        # We also do the same
        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumExceptions()):
                    particle1, particle2, chargeProd, sigma, epsilon = force.getExceptionParameters(i)
                    if (particle1 in idx) & (particle2 in idx):
                        if chargeProd != 0 * u.elementary_charge**2: # Is it a 1-4 interaction?
                            new_sigma = (sigmas[particle1] + sigmas[particle2]) / 2
                            new_epsil = np.sqrt(epsilons[particle1] * epsilons[particle2]) / 2
                            force.setExceptionParameters(i, particle1, particle2,
                                                         chargeProd,
                                                         new_sigma.in_units_of(u.angstrom),
                                                         new_epsil.in_units_of(u.kilojoule_per_mole))



    def adjust_solute_charges(self, idx, new_charges, scale_fac=1.2, verbose=True):
        ''' idx: indices of solute, corresponding to new_charges '''
        ### MANUALLY ADJUST
        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumParticles()):
                    if i not in idx:
                        continue
                    charge, sigma, epsilon = force.getParticleParameters(i)
                    force.setParticleParameters(i, new_charges[i] * u.elementary_charge, sigma, epsilon)
                    if verbose:
                        print(f'OLD: {charge._value:9.6f}, NEW: {new_charges[i]:9.6f}')

        ### 1-4 SCALING: Only for AMBER - TODO: make it possible to turn it off.
        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumExceptions()):
                    particle1, particle2, chargeProd, sigma, epsilon = force.getExceptionParameters(i)
                    if (particle1 in idx) & (particle2 in idx):
                        if chargeProd != 0 * u.elementary_charge**2: # Is it a 1-4 interaction?
                            newChargeProd = new_charges[particle1] * new_charges[particle2] / scale_fac
                            force.setExceptionParameters(i, particle1, particle2,
                                                         newChargeProd * u.elementary_charge**2, sigma, epsilon)

    def add_constraint(self, i, j, dist):
        """ dist should be in angstrom """
        self.system.addConstraint(i, j, dist)

    def check_bonds(self, idx):
        ''' Return all bonds of atoms in the idx list '''
        for force in self.system.getForces():
            if isinstance(force, HarmonicBondForce):
                hbf = force
        syms = [a.name for a in self.topology.atoms()]
        bonds = []
        for bond in range(hbf.getNumBonds()):
            a, b, this_d, this_k = hbf.getBondParameters(bond)
            if (a in idx) or (b in idx):
                d_ang = this_d.in_units_of(u.angstroms)._value
                k_kjmolang = this_k.in_units_of(u.kilojoule_per_mole / u.angstroms**2)._value
                print(f'{a:2d},{b:2d}: {syms[a]:2s}-{syms[b]:2s}: Dist: {d_ang:6.4f} Ang. k = {k_kjmolang:g} kj/mol*ang**2')
                bonds.append((a, b, d_ang, k_kjmolang, syms[a] + syms[b]))

        if not bonds:
            print('No bonds found')

        return bonds


    def add_bond(self, i, j, dist, k):
        ''' Simple hookean bond with spring constant k between
            atoms of indices i and j '''
        bond = HarmonicBondForce()
        bond.addBond(i, j, dist, k)
        self.system.addForce(bond)

    def update_bond(self, i, j, dist=None, k=None, do_print=True):
        ''' Update already existing bond distance and/or spring constant.
            set k='None' if you want to remove bon to remove bond '''
        syms = [a.name for a in self.topology.atoms()]

        for force in self.system.getForces():
            if isinstance(force, HarmonicBondForce):
                hbf = force
        exist = False
        for bond in range(hbf.getNumBonds()):
            a, b, this_d, this_k = hbf.getBondParameters(bond)
            if (a == i and b == j) or (b == i and a == j):
                exist = True
                if dist and k:
                    if k == 'None':
                        k = this_k * 0
                    hbf.setBondParameters(bond, a, b, dist, k)
                    print('dist and k')
                elif dist:
                    hbf.setBondParameters(bond, a, b, dist, this_k)
                    print('dist')
                elif k:
                    if k == 'None':
                        k = this_k * 0
                    hbf.setBondParameters(bond, a, b, this_d, k)
                    print('k')
        if (not exist) and do_print:
            print(f'Bond between {syms[i]}-{syms[j]} didn\'t exist, please use add_bond')
        elif exist:
            print(f'Bond between {syms[i]}-{syms[j]} updated to {k}')

        if hasattr(self.simulation, 'context'):
            print('UPDATING CONTEXT')
            hbf.updateParametersInContext(self.simulation.context)

    def get_bond(self, i, j):
        ''' Get bond parameters between i and j '''
        syms = [a.name for a in self.topology.atoms()]

        this_d = None
        this_k = None
        for force in self.system.getForces():
            if isinstance(force, HarmonicBondForce):
                hbf = force
        for bond in range(hbf.getNumBonds()):
            a, b, d, k = hbf.getBondParameters(bond)
            if (a == i and b == j) or (b == i and a == j):
                this_d = d
                this_k = k
        if not this_d:
            print(f'Bond between {syms[i]}-{syms[j]} didn\'t exist, please use add_bond')

        return this_d, this_k, syms[a] + syms[b]

    def update_angle(self, i, j, k, angle=None, kf=None, do_print=True):
        ''' Update angle params between atoms a, b, c.
            Dont forget to use Quantities when setting. '''
        syms = [a.name for a in self.topology.atoms()]
        for force in self.system.getForces():
            if isinstance(force, HarmonicAngleForce):
                abf = force
        exist = False
        for bond in range(abf.getNumAngles()):
            a, b, c, this_angle, this_k = abf.getAngleParameters(bond)
            if (a == i) and (b == j) and (c == k):
                exist = True
                #print(this_angle, this_k)
                if angle:
                    abf.setAngleParameters(bond, a, b, c, angle, this_k)
                if kf:
                    abf.setAngleParameters(bond, a, b, c, this_angle, kf)
                if angle and kf:
                    abf.setAngleParameters(bond, a, b, c, angle, kf)
        if (not exist) and do_print:
            print(f'Angle between {syms[i]}-{syms[j]}-{syms[k]} didn\'t exist')
        elif exist:
            print(f'Angle between {syms[i]}-{syms[j]}-{syms[k]} updated')
        for force in self.system.getForces():
            if isinstance(force, HarmonicAngleForce):
                print('Updating object')
                force = abf

    def dissociate(self, atoms_in_A, atoms_in_B, verbose=True):
        ''' Set all bonded force constants between atoms in A and B to 0.
            Re-establish normal LJ/charge interactions as if the two fragments A and B
            were not connected.

            This means that if you also want to change charges and LJ parameters to
            something completely different (reflecting e.g. an electronically excited
            state), you should do that AFTER calling this function.
        '''

        for force in self.system.getForces():
            if isinstance(force, HarmonicBondForce):
                # Remove any bond that crosses A↔B
                for bond_index in reversed(range(force.getNumBonds())):
                    (i, j, r0, k) = force.getBondParameters(bond_index)
                    # Check if i and j live in different sets
                    inA = (i in atoms_in_A)
                    inB = (i in atoms_in_B)
                    j_inA = (j in atoms_in_A)
                    j_inB = (j in atoms_in_B)
                    if (inA and j_inB) or (inB and j_inA):
                        idx1, idx2, r0, k = force.getBondParameters(bond_index)
                        force.setBondParameters(bond_index, idx1, idx2, r0, k*0)

            elif isinstance(force, HarmonicAngleForce):
                # Remove any angle that crosses A↔B
                for angle_index in reversed(range(force.getNumAngles())):
                    (i, j, k, theta0, ktheta) = force.getAngleParameters(angle_index)
                    sets = [ (i in atoms_in_A), (i in atoms_in_B),
                            (j in atoms_in_A), (j in atoms_in_B),
                            (k in atoms_in_A), (k in atoms_in_B) ]
                    # If this angle includes atoms in both A and B, remove it
                    if any(sets[0:2]) and any(sets[2:4]) and any(sets[4:6]):
                        # That check might need to be more explicit, e.g.
                        # "if (i in A and j in B) or (i in B and j in A) ..."
                        # plus the third atom. But the idea is the same.
                        idx1, idx2, idx3, ang, k = force.getAngleParameters(angle_index)
                        force.setAngleParameters(angle_index, idx1, idx2, idx3, ang, k*0)


            elif isinstance(force, PeriodicTorsionForce):
                # Remove any torsion that crosses A↔B
                for torsion_index in reversed(range(force.getNumTorsions())):
                    (i, j, k, l, periodicity, phase, k_torsion) = \
                        force.getTorsionParameters(torsion_index)
                    # If i, j, k, l are not all in the same group => remove
                    # A simple check:
                    groups = [
                        (i in atoms_in_A), (j in atoms_in_A),
                        (k in atoms_in_A), (l in atoms_in_A),
                        (i in atoms_in_B), (j in atoms_in_B),
                        (k in atoms_in_B), (l in atoms_in_B)
                    ]
                    # If some are in A and some are in B, remove
                    if (any(groups[0:4]) and any(groups[4:8])):
                        idx1, idx2, idx3, idx4, idx5, ang, k = force.getTorsionParameters(torsion_index)
                        force.setTorsionParameters(torsion_index, idx1, idx2, idx3, idx4, idx5, ang, k*0)

        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                # We might need the per-particle parameters to reconstruct normal LJ/charge
                # for each atom, so we can combine them. Let's fetch them now:
                q_sigma_epsilon = [force.getParticleParameters(i) for i in range(self.system.getNumParticles())]
                # q_sigma_epsilon[i] = (charge, sigma, epsilon)

                for ex_index in range(force.getNumExceptions()):
                    i, j, chargeProd, sigma, epsilon = force.getExceptionParameters(ex_index)
                    # If (i in A and j in B) or (i in B and j in A), revert to normal
                    if ((i in atoms_in_A and j in atoms_in_B) or
                        (i in atoms_in_B and j in atoms_in_A)):
                        # Re-compute the "normal" parameters for pair (i, j):
                        charge_i, sigma_i, eps_i = q_sigma_epsilon[i]
                        charge_j, sigma_j, eps_j = q_sigma_epsilon[j]
                        # full coulomb: chargeProd = charge_i * charge_j
                        new_chargeProd = charge_i*charge_j

                        # combine sigmas/eps by standard combining rules (Lorentz-Berthelot):
                        new_sigma  = 0.5*(sigma_i + sigma_j)
                        new_epsilon = (eps_i*eps_j)**0.5

                        # For a completely normal (unscaled) pair:
                        force.setExceptionParameters(ex_index, i, j,
                                                    new_chargeProd, new_sigma, new_epsilon)

                        if verbose:
                            print(i, j, f'before had {chargeProd}, new charge prod: {new_chargeProd}',
                                        f'{sigma}, {epsilon} --> {new_sigma}, {new_epsilon}')


    def update_solute_geometry(self, new_atoms):
        ''' Updates the geometry of the solute to the geometry 
            of the ASE Atoms object new_atoms, which should contain your solute. 

            Will mess around with the positions of the first len(new_atoms) 
            entries in self.positions

            Will first try to translate and Kabsch-rotate new_atoms
            to maximize overlap with original positions. Should reduce risk
            of your system blowing up after doing this (but not eliminate it entirely)

        '''
        idx = list(range(len(new_atoms)))
        num_atoms = len(list(self.topology.atoms())) 
        org_atoms = self.to_ase(idx)  # get solute

        # kabsch alignment
        for _ in range(10):
            ref_pos = org_atoms.get_positions()
            atm_pos = new_atoms.get_positions()

            # Translate atoms to ref
            ref_c = rmsd.centroid(ref_pos)
            atm_c = rmsd.centroid(atm_pos)
            atm_pos += (ref_c - atm_c)

            # Get rotation matrix via the Kabsch algo
            U = rmsd.kabsch(ref_pos, atm_pos)

            pos = new_atoms.get_positions() + (ref_c - atm_c)
            new_pos = np.dot(pos, U.T)

            new_atoms.set_positions(new_pos)

        # update positions
        test = self.positions[0]
        if hasattr(test, 'in_units_of'):
            pos = np.array(self.positions.value_in_unit(u.angstrom))
        else:
            pos = self.positions  # we assume it's just an angstrom nparray

        pos[idx] = new_atoms.get_positions()
        self.update_positions(pos)



    def pretty_hard_wall(self, indices, sigma):
        wall = CustomNonbondedForce("4*epsilon*((sigma/r)^12); sigma=0.5*(sigma1+sigma2); epsilon=sqrt(epsilon1*epsilon2)");
        wall.addPerParticleParameter("sigma");
        wall.addPerParticleParameter("epsilon");
        for idx in indices:
            wall.addParticle(idx)

        self.system.addForce(wall)

    def restrain_atoms(self, indices, k=500, pos=None):
        ''' Restrain atoms with indices to the positions in self
            or pos, with the Hookean constant k, in units of  kJ/(mol * ang**2)
        '''

        k *= u.kilojoule_per_mole / u.angstrom**2

        energy_expression = '(K/2)*periodicdistance(x, y, z, x0, y0, z0)^2' # periodic distance
        restraint_force = CustomExternalForce(energy_expression)
        restraint_force.addGlobalParameter('K', k)
        restraint_force.addPerParticleParameter('x0')
        restraint_force.addPerParticleParameter('y0')
        restraint_force.addPerParticleParameter('z0')
        for idx in indices:
            if pos is not None:
                restraint_force.addParticle(idx, pos)
            else:
                res_pos = self.positions[idx].value_in_unit_system(md_unit_system)
                restraint_force.addParticle(idx, res_pos)

        self.system.addForce(restraint_force)


    def add_morse(self, indices, D, a, r0):
        morse = mm.CustomBondForce("D*(1-exp(a*(r0-r)))^2")
        morse.addPerBondParameter('D')
        morse.addPerBondParameter('a')
        morse.addPerBondParameter('r0')

        morse.addBond(indices[0], indices[1], [D, a, r0])
        self.system.addForce(morse)


    def flat_bottom_repulsive(self, idx1, idx2, r0, k):
        ''' Repulsive potential keeping away atoms
            of indices idx1 and idx2 when they get
            below r0, with a force constant of k,
            in units of  kJ/(mol * ang**2)
        '''

        k *= u.kilojoule_per_mole / u.angstrom**2


        fbr = mm.CustomBondForce(
            'step(r0-r) * (k/2) * (r0-r)^2')
        fbr.addGlobalParameter('r0', r0)
        fbr.addGlobalParameter('k', k)

        fbr.setUsesPeriodicBoundaryConditions(True)

        self.system.addForce(fbr)
        for i, j in zip(idx1, idx2):
            #fbr.addBond(i, j, [r0, k])
            fbr.addBond(i, j)

    def to_ase(self, idxs=None):
        ''' Convert all atoms with indexes in the list idxs
            to an ASE atoms object.
        '''
        from ase import Atoms

        # Virtual particles will be 'He'
        els = [a.element.symbol if a.element else 'He'
                        for a in self.topology.atoms() ]

        if self.positions is None:
            raise RuntimeError('System as no positions')
        test = self.positions[0]
        if hasattr(test, 'in_units_of'):
            pos = np.vstack([np.array(x.in_units_of(u.angstrom)._value)
                                for x in self.positions])
        else:
            pos = self.positions  # we assume it's just an angstrom nparray

        # update positions if system has run steps
        if hasattr(self.simulation, 'context'):
            state = self.simulation.context.getState(getPositions=True,
                                                     getVelocities=False,
                                                     getEnergy=False)  # WIP: more to ase
            pos = state.getPositions()
            pos = np.vstack([np.array(x.in_units_of(u.angstrom)._value)
                    for x in pos])
            print('using positions from context')

        atoms = Atoms(els, positions=pos)

        box_vectors = self.system.getDefaultPeriodicBoxVectors()
        if box_vectors:
            cell = [getattr(bv.value_in_unit(u.angstrom), attr)
                    for bv, attr in zip(box_vectors, ['x', 'y', 'z'])]
            atoms.set_cell(cell)

        charges, sigmas, epsilons = self.get_nonbonded(idx=list(range(len(atoms))))
        charges = [c._value for c in charges]

        atoms.set_initial_charges(charges)

        if idxs:
            return atoms[idxs]
        else:
            return atoms

    def to_mdanalysis(self):
        ''' Convert the system to an MDAnalysis Universe object 
            Requires MDAnalysis to be installed.
        '''
        from MDAnalysis import Universe

        # update positions if system has run steps
        if hasattr(self.simulation, 'context'):
            state = self.simulation.context.getState(getPositions=True,
                                                     getVelocities=False,
                                                     getEnergy=False)
            pos = state.getPositions()
        else:
            pos = self.positions 
        uni = Universe(self.topology, pos)
        return uni

    def get_nonsolvent_indices(self, tag='Na+'):
        ''' Assumes topology is ordered thusly: solute,ction,solvent and gives
            you all nonsolvent indices and symbols based on the ction symbol '''

        atom_symbols = [a.name for a in self.topology.atoms()]
        last = [idx for idx, s in enumerate(atom_symbols) if s==tag][-1] + 1
        idx = list(range(last))
        syms = [atom_symbols[i] for i in idx]

        return idx, syms


    def add_barostat(self, pressure=1, temperature=300):
        self.system.addForce(MonteCarloBarostat(pressure * u.bar, temperature * u.kelvin))


    def set_integrator(self, ensemble='NVT', temperature=300, fc=1, timestep=2):
        """ Only Langevin so far.
            fc = friction in 1/ps
            temperature in Kelvin
            timestep in fs. """

        if ensemble == 'NVT':
            # pylint: disable=maybe-no-member
            self.integrator = LangevinIntegrator(temperature * u.kelvin,
                                                 fc / u.picosecond,
                                                 timestep * u.femtoseconds)
        elif ensemble == 'NVE':
            self.integrator = VelocityVerletIntegrator(timestep = timestep * u.femtoseconds)


        self.timestep = timestep


    def init_simulation(self, box_vectors=None, positions=None, platform=None,
                        double=False, pbcbonds=True):
        ''' When you do this, charges, custom forces and constraints
            CANNOT be changed afterwards. '''

        if not positions:
            positions = self.positions

        # this is off by default, since their proteins are too big to diffuse
        # from image to image, i guess? Maybe that is why things randomly
        # blow up..
        if pbcbonds:
            for force in self.system.getForces():
                if isinstance(force, HarmonicBondForce):
                    force.setUsesPeriodicBoundaryConditions(True)
                elif isinstance(force, HarmonicAngleForce):
                    force.setUsesPeriodicBoundaryConditions(True)
                elif isinstance(force, PeriodicTorsionForce):
                    force.setUsesPeriodicBoundaryConditions(True)


        # TODO: kwargs...
        if platform:
            'OpenCL, CUDA, CPU, or Reference'
            platform = Platform.getPlatformByName(platform)
            if double:
                properties = {'CudaPrecision':'double'}
                self.simulation = app.Simulation(self.topology, self.system,
                                                 self.integrator, platform, properties)
            else:
                self.simulation = app.Simulation(self.topology, self.system,
                                                 self.integrator, platform)
        else:
            self.simulation = app.Simulation(self.topology, self.system, self.integrator)

        self.simulation.context.setPositions(positions)

        # TODO: get boxvectors from prmtop or crd or wherever.f
        if box_vectors:
            self.simulation.context.setPeriodicBoxVectors(*box_vectors)
        elif self.boxvectors:  # loaded from rst7
            print('using rst7/dcd boxvectors')
            self.simulation.context.setPeriodicBoxVectors(*self.boxvectors)

        if self.velocities:  # set velocities to state saved in self
            self.simulation.context.setVelocities(*self.velocities)


    def minimize(self, **kwargs):
        self.simulation.minimizeEnergy(**kwargs)


    def mbd(self, temperature=300):
        ''' Maxwell-Boltzmann distribute velocities. '''
        self.simulation.context.setVelocitiesToTemperature(temperature * u.kelvin)


    def standard_reporters(self, step=500, to_screen=True):
        self.simulation.reporters.append(app.DCDReporter(os.path.join(self.fpath, self.tag) + '.dcd', step))
        olist = [os.path.join(self.fpath, self.tag) + '.log']

        if to_screen:
            olist = [stdout] + olist
        for out in olist:
            self.simulation.reporters.append(app.StateDataReporter(out, step, step=True, speed=True,
                                             potentialEnergy=True, temperature=True, volume=True,
                                             density=True, separator='\t'))

    def add_reporter(self, rep):
        ''' add custom reporter object '''
        self.simulation.reporters.append(rep)

    def run(self, time_in_ns=None, steps=None, time_in_ps=None):
        ''' Somewhat silly. Only fill out one of them ok. '''

        # TODO: Dont be silly. Or add some checks.
        if steps:
            runsteps = steps
        if time_in_ns:
            runsteps = int(time_in_ns * 1e6 / self.timestep)
        if time_in_ps:
            runsteps = int(time_in_ps * 1e3 / self.timestep)

        self.simulation.step(runsteps)
        o = os.path.join(self.fpath, self.tag)
        self.simulation.saveState(o + '.xml')
        restrt = RestartReporter(o +  '.rst7', 10000)
        # pylint: disable=unexpected-keyword-arg
        state = self.simulation.context.getState(getPositions=True, # pylint: disable=no-value-for-parameter
                                                 getVelocities=True,
                                                 enforcePeriodicBox=True)
        restrt.report(self.simulation, state)

