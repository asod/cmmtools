''' Create a working OpenMM System with:
    1. No bonds, angles, dihedrals or impropers within the solute
    3. Solvate it in a pre-established solvent box.
    4. Use CMMSystems to update its nonbonded parameters.
'''
import numpy as np
from openmmtools.testsystems import TestSystem
from openmm import app, unit
from openmm import openmm as mm
from openmm.vec3 import Vec3
from ase.io import read, write
from MDAnalysis import Universe
from collections import defaultdict
from sys import stdout
from parmed.openmm.reporters import RestartReporter
import parmed as pmd
from .gmx_tools import (top_as_itp, create_system_top, run_gmx_commands,
                        get_resname, replace_resname)
import shutil


# Nonbonded parameters from gaff.
# Taken from FeBPY parametrization.
# Not that all Carbon atom types apart from c3, c1, cg, ch
# have same nonbonded params. FeBPY did not contain any of these
# XXX WIP: Better (i.e. ANY kind of) atom typing apart from simple
# elements.
#                eps (kj/mol),  sigma (nm)
gaff_wip = {'C':  ('0.359824',  '0.339967'),
            'N':  ('0.711280',  '0.325000'),
            'H':  ('0.062760',  '0.259964'),   # WIP: Different H atom types!!
            'Fe': ('0.07200664','0.251055'),  # From MCPB.py
            'Cl': ('2.730887',  '0.413555'),  # From MCPB.py - water
            'Cu': ('263.592',   '0.046000'),  #Cu+ in ACN 10.1021/jp402545g - so CHANGE if you want water
            'P':  ('0.836800',  '0.374177'),  # From AgPtPOP so GAFF
            'O':  ('0.711280',  '0.300000')   # Ditto. BUT there is at least two O-types...
            }

# you can be lucky to find more here: ~/anaconda3/envs/clean/lib/python3.9/site-packages/openmm/app/data/amber14
# or wherever you've installed openmm

class EmptyInside(TestSystem):
    def __init__(self, tag, **kwargs):
        # gives you self.serialize(), self.topology()
        # and self._system
        TestSystem.__init__(self, **kwargs)

        self.tag = tag  # name used in output

        self.u_pos = None  # Solute positions
        self.solu_xml = None  # solute FF template

        self.solu = None # Ase Atoms object of solute
        self.u_bonds = [] # Solute bonds

        self.modeller = None # object that can combine tops, aquate...
        self.positions = None
        self.simulation = None

        self.residues = []  #
        self.ct_ions = []

    def add_solute(self, atoms, neutralize=False):
        ''' Create solute topology from ase Atoms object.

            neutralize: (ASE Atoms)
                Only use for non-water solvents.
                a counterion residue will be added based on the
                charges, positions and symbols of the Atoms object.

                add_water() can automatically neutralize, so with
                water you less hassle
        '''
        if isinstance(atoms, str):
            atoms = read(atoms)

        self.u_pos = atoms.get_positions()
        self.solu = atoms

        # Add chain and residue to topology
        chain = self.topology.addChain()
        residue = self.topology.addResidue('mol', chain)

        self.residues.append(residue)

        # Add atoms to system and topology
        top_atoms = []
        for atom in atoms:
            self._system.addParticle(atom.mass)
            top_atoms.append(self.topology.addAtom(atom.symbol,
                                  app.Element.getBySymbol(atom.symbol),
                                  residue))

        # Guess bonds and add them to the topology
        write('tmp_solute.xyz', atoms, plain=True)
        u = Universe('tmp_solute.xyz')
        u.atoms.guess_bonds(vdwradii={'FE':2.5, 'CU':3})  # wip more unknowns
        for b in u.atoms.bonds:
            a1, a2 = b.indices
            self.topology.addBond(top_atoms[a1], top_atoms[a2])

            self.u_bonds += [(a1, a2)]

        if neutralize is not False:
            self._add_counterions(neutralize)

    def _add_counterions(self, ions):
        ''' adds the ASE Atoms object "ions" to topology
        '''
        charge = '-' if ions[0].charge < 0 else '+'
        resname = ions[0].symbol + charge
        chain = [chain for chain in self.topology.chains()][0]
        residue = self.topology.addResidue(resname, chain)
        ct_ions = []
        for ion in ions:
            element = app.Element.getBySymbol(ion.symbol)
            self._system.addParticle(element.mass)
            ct_ions.append(self.topology.addAtom(ion.symbol,
                           element,
                           residue))

        self.residues.append(residue)
        self.ct_ions = ions

    def init_poor_mans_gaff(self, solvent='amber14/tip4pew.xml', charges=None):
        ''' Creates an xml Force Field template based on the solute.

            charges: (#atoms, ) np.array
                partial charges
        '''
        if self.solu_xml is None:
            self._generate_solute_xml(charges=charges)
        with open('tmp_solu.xml', 'w') as f:
            f.write(self.solu_xml)

        ffs = ['tmp_solu.xml']
        if solvent:
            ffs += [solvent]
        self.ff = app.ForceField(*ffs)  # WIP: more solvents

        self.system = self.ff.createSystem(self.topology,
                                      nonbondedCutoff=1 * unit.nanometer,
                                      rigidWater=True)

    def get_pmd_structure(self):
        pos = self.u_pos  # pre-solvent
        if self.modeller:  # solvent is added, and self.topology will be from modeller
            # WIP might need to convert to numpy in angstrom
            pos = self.modeller.getPositions()
        if self.ct_ions:
            pos = np.vstack((self.u_pos,
                             self.ct_ions.positions))

        struc = pmd.openmm.load_topology(self.topology,
                                         system=self._system,
                                         xyz=pos)
        return struc

    def export_to_gmx(self, overwrite=True):
        struc = self.get_pmd_structure()
        struc.save(self.tag + '.top', overwrite=overwrite)
        struc.save(self.tag + '.gro', overwrite=overwrite)

    def export_to_amber(self, overwrite=True):
        struc = self.get_pmd_structure()
        struc.save(self.tag + '.prmtop', overwrite=overwrite)
        struc.save(self.tag + '.inpcrd', overwrite=overwrite)

    def add_gmx_solvent(self, v_top_path, v_name, v_gro_path, boxside_ang=75):
        ''' Uses gmx solvate to generate solvated system.top, .gro, and
            two .itp files, one for solvent, one for solute.

            These can then beloaded into CMMSystems with
            CMMSystems().system_from_gromacs()

            REQUIRES a working installation of gromacs.

            You need a .gro and .top of your small organic molecule-
            solvent of choice. Since the LJ parameters of your solute
            are most likely GAFF if you've used this class, be sure to
            also get the GAFF-small molecules:
            https://virtualchemistry.org/ff/GAFF-ESP-2018.zip
        '''

        self.export_to_gmx()

        u_itp = top_as_itp(self.tag + '.top')
        v_itp = top_as_itp(v_top_path)

        oldres = self.residues[0].name #get_resname(u_itp)
        fixed_itp = replace_resname(u_itp, oldres, 'XYZ')

        with open(self.tag + '.itp', 'w') as f:
            f.write(fixed_itp)

        with open(v_name + '.itp', 'w') as f:
            f.write(v_itp)

        ct_ions = None
        if self.ct_ions:
            num_ct = len(self.ct_ions)
            resname = self.residues[-1].name  # XXX WIP: assuming it's the last... dirty
            ct_ions = f'{resname:19s}{num_ct:d}'

        systop = create_system_top(fixed_itp, v_itp, self.tag, v_name,
                                   counterions=ct_ions)

        with open('system.top', 'w') as f:
            f.write(systop)

        # copy solvent .gro in here
        shutil.copy2(v_gro_path, v_name + '.gro')

        # run gmx
        run_gmx_commands(v_name, self.tag, boxside_ang)


    def add_water(self, model='tip4pew', nummols=4096):
        modeller = app.Modeller(self.topology,
                                unit.quantity.Quantity(self.u_pos,
                                                       unit=unit.angstroms)
                                )
        modeller.addSolvent(self.ff, model=model, numAdded=nummols)

        self.system = self.ff.createSystem(modeller.topology,
                                           nonbondedMethod=app.PME,
                                           constraints=app.HBonds)

        self.modeller = modeller
        self.topology = self.modeller.topology

    def set_cell(self, cell):
        x, y, z = cell
        boxvectors = (Vec3(x, 0, 0) * unit.angstrom,
                      Vec3(0, y, 0) * unit.angstrom,
                      Vec3(0, 0, z) * unit.angstrom)

        self.system.setDefaultPeriodicBoxVectors(*boxvectors)

    def restrain_solute(self, k=1000):
        ## Restrain solute
        k *= unit.kilojoule_per_mole / unit.angstrom**2
        energy_expression = '(K/2)*periodicdistance(x, y, z, x0, y0, z0)^2' # periodic distance
        restraint_force = mm.CustomExternalForce(energy_expression)
        restraint_force.addGlobalParameter('K', k)
        restraint_force.addPerParticleParameter('x0')
        restraint_force.addPerParticleParameter('y0')
        restraint_force.addPerParticleParameter('z0')
        pos = self.modeller.getPositions()
        for idx in range(len(self.solu)):
            res_pos = pos[idx]
            restraint_force.addParticle(idx, res_pos)
        self.system.addForce(restraint_force)

    def init_simulation(self, ensemble='NPT', temperature=300, fc=1, timestep=2, pressure=1):
        self.timestep = timestep
        if ensemble == 'NPT':
            self.system.addForce(mm.MonteCarloBarostat(pressure * unit.bar,
                                                       temperature * unit.kelvin))
        if ensemble == 'NVE':
            integrator = mm.VelocityVerletIntegrator(timestep=timestep * unit.femtoseconds)
        else:
            integrator = mm.LangevinMiddleIntegrator(temperature * unit.kelvin,
                                                    fc/unit.picosecond,
                                                    timestep * unit.picoseconds)
        simulation = app.Simulation(self.modeller.topology, self.system, integrator)
        simulation.context.setPositions(self.modeller.getPositions())

        self.simulation = simulation

    def standard_reporters(self, step=500, to_screen=True):
        self.simulation.reporters.append(app.DCDReporter(self.tag + '.dcd', step))
        olist = [self.tag + '.log']

        if to_screen:
            olist = [stdout] + olist
        for out in olist:
            self.simulation.reporters.append(app.StateDataReporter(out, step, step=True, speed=True,
                                             potentialEnergy=True, temperature=True, volume=True,
                                             density=True, separator='\t'))

    def add_reporter(self, rep):
        ''' add custom reporter object '''
        self.simulation.reporters.append(rep)

    def run(self, time_in_ns=1, steps=None, time_in_ps=None):
        ''' Somewhat silly. Only fill out one of them ok. '''

        # TODO: Dont be silly. Or add some checks.
        if steps:
            runsteps = steps
        if time_in_ns:
            runsteps = int(time_in_ns * 1e6 / self.timestep)
        if time_in_ps:
            runsteps = int(time_in_ps * 1e3 / self.timestep)

        self.simulation.step(runsteps)
        self.simulation.saveState(self.tag + '.xml')
        restrt = RestartReporter(self.tag + '.rst7', 10000)
        state = self.simulation.context.getState(getPositions=True,
                                                 getVelocities=True,
                                                 enforcePeriodicBox=True)
        restrt.report(self.simulation, state)

        with open(f'{self.tag}_ei_serialized.xml', 'w') as f:
            for line in self.serialize():
                f.write(line)

    def _generate_solute_xml(self, charges=None):
        if charges is None:
            charges = np.zeros(len(self.solu))
        u_resname = self.residues[0].name
        xml = f'''\
<ForceField>
<Residues>
  <Residue name="{u_resname}">
'''
        # <Residues>
        for i, atom in enumerate(self.solu):
            xml += f'   <Atom name="{atom.symbol}{i:03d}" type="{u_resname}-{atom.symbol}{i:03d}"/>\n'

        for i, bond in enumerate(self.u_bonds):
            a1, a2 = bond
            xml += f'   <Bond from="{a1}" to="{a2}"/>\n'
        xml += ' </Residue>\n'

        if len(self.residues) > 1:  # counterions are added
            for res in self.residues[1:]:  # if there's more than 1 this is weird
                xml += f'  <Residue name="{res.name}">\n'
                for i, atom in enumerate(self.ct_ions):
                    xml += f'   <Atom name="{atom.symbol}{i:03d}" type="{res.name}-{atom.symbol}{i:03d}"/>\n'
                xml += ' </Residue>\n'

        xml += '</Residues>\n'

        # <Atomtypes> - per element for now.
        xml +='<AtomTypes>\n'
        #unique_syms = np.unique(self.solu.get_chemical_symbols())
        for i, atom in enumerate(self.solu):
            mass = atom.mass
            sym = atom.symbol
            xml += f'  <Type name="{u_resname}-{sym}{i:03d}" class="{u_resname}-{sym}{i:03d}" element="{sym}" mass="{mass}"/>\n'

        if len(self.residues) > 1:
            for res in self.residues[1:]:  # if there's more than 1 this is weird
                for i, atom in enumerate(self.ct_ions):
                    mass = atom.mass
                    sym = atom.symbol
                    xml += f'  <Type name="{res.name}-{sym}{i:03d}" class="{res.name}-{sym}{i:03d}" element="{sym}" mass="{mass}"/>\n'

        xml += '</AtomTypes>\n'

        # <NonbondedForce>
        xml += '<NonbondedForce coulomb14scale="0.8333333333333334" lj14scale="0.5">\n'

        for i, atom in enumerate(self.solu):
            sym = atom.symbol
            eps, sig = gaff_wip[sym]
            charge = charges[i]
            xml += f'  <Atom type="{u_resname}-{atom.symbol}{i:03d}" charge="{charge}" sigma="{sig}" epsilon="{eps}"/>\n'
        if len(self.residues) > 1:
            ct_charges = self.ct_ions.get_initial_charges()
            for res in self.residues[1:]:  # if there's more than 1 this is weird
                for i, atom in enumerate(self.ct_ions):
                    sym = atom.symbol
                    eps, sig = gaff_wip[sym]
                    charge = ct_charges[i]
                    xml += f'  <Atom type="{res.name}-{atom.symbol}{i:03d}" charge="{charge}" sigma="{sig}" epsilon="{eps}"/>\n'

        xml += '</NonbondedForce>\n'

        xml += '<HarmonicBondForce>\n'
        for bond in self.u_bonds:
            a1, a2 = bond
            s1, s2 = self.solu[a1].symbol, self.solu[a2].symbol
            d = self.solu.get_distance(a1, a2) / 10 # nm
            xml += f'<Bond class1="{u_resname}-{s1}{a1:03d}" class2="{u_resname}-{s2}{a2:03d}" length="{d}" k="500000"/>\n'
        xml += '</HarmonicBondForce>\n'

        xml += '</ForceField>'

        self.solu_xml = xml

    def to_ase(self, idxs=None):
        ''' Convert all atoms with indexes in the list idxs
            to an ASE atoms object.
        '''
        from ase import Atoms

        # Virtual particles will be 'He'
        els = [a.element.symbol if a.element else 'He'
                        for a in self.topology.atoms() ]

        if self.positions is None:
            try:
                self.positions = self.modeller.getPositions()
            except:
                raise RuntimeError('System as no positions')
        test = self.positions[0]
        if hasattr(test, 'in_units_of'):
            pos = np.vstack([np.array(x.in_units_of(unit.angstrom)._value)
                                for x in self.positions])
        else:
            pos = self.positions  # we assume it's just an angstrom nparray

        # update positions if system has run steps
        if self.simulation is not None:
            if hasattr(self.simulation, 'context'):
                state = self.simulation.context.getState(getPositions=True,
                                                        getVelocities=False,
                                                        getEnergy=False)  # WIP: more to ase
                pos = state.getPositions()
                pos = np.vstack([np.array(x.in_units_of(unit.angstrom)._value)
                        for x in pos])
                print('using positions from context')

        atoms = Atoms(els, positions=pos)

        box_vectors = self.system.getDefaultPeriodicBoxVectors()
        if box_vectors:
            cell = [getattr(bv.value_in_unit(unit.angstrom), attr)
                    for bv, attr in zip(box_vectors, ['x', 'y', 'z'])]
            atoms.set_cell(cell)

        #charges, sigmas, epsilons = self.get_nonbonded(idx=list(range(len(atoms))))
        #charges = [c._value for c in charges]

        #atoms.set_initial_charges(charges)

        if idxs:
            return atoms[idxs]
        else:
            return atoms





def define_angles_torsions(bonds):
    ''' Returns indices of angles and dihedrals from list of bond-tuples.
        ALL this hinges on the bond-guessing, so it's NOT GUARANTEED to
        be chemical AT ALL.

        But simply applying positional restraints required too high
        force constants for a 2 fs timestep to work.

        WIP: Actually use me.
    '''
    bond_dict = defaultdict(list)
    for a, b in bonds:
        bond_dict[a].append(b)
        bond_dict[b].append(a)

    # Step 2: Compute angles
    angles = []
    for center in bond_dict:
        neighbors = bond_dict[center]
        for i in range(len(neighbors)):
            for j in range(i + 1, len(neighbors)):
                angles.append((neighbors[i], center, neighbors[j]))

    # Step 3: Compute torsions
    torsions = []
    for first in bond_dict:
        for second in bond_dict[first]:
            if first < second:  # This condition avoids double counting each torsion
                for third in bond_dict[second]:
                    if third != first:  # Avoid backtracking
                        for fourth in bond_dict[third]:
                            if fourth != second and fourth not in bond_dict[first]:  # Ensure valid sequence
                                torsions.append((first, second, third, fourth))
    return angles, torsions



def find_closest_product(x):
    ''' Was used for creating a water grid.. Currently not used'''
    # Estimate a range based on cube root of x
    n = int(np.cbrt(x)) + 1

    # Initialize variables to store the best triplet and the minimum absolute difference found
    best_triplet = (0, 0, 0)
    min_abs_diff = float('inf')
    best_diff = 0  # To store the signed difference

    # Check all combinations of i, j, k from 1 to n
    for i in range(1, n+1):
        for j in range(1, n+1):
            for k in range(1, n+1):
                product = i * j * k
                diff = product - x  # Calculate signed difference

                # Update the best triplet if a closer product is found based on absolute difference
                if abs(diff) < min_abs_diff:
                    min_abs_diff = abs(diff)
                    best_triplet = (i, j, k)
                    best_diff = diff  # Store the signed difference

    return best_triplet, best_diff
