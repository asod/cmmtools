''' Simple system classes, mainly taken from OpenMMTools and modified slightly. '''
from openmmtools.testsystems import TestSystem
from openmmtools.testsystems import in_openmm_units, build_lattice, generate_dummy_trajectory, subrandom_particle_positions
import parmed

try:
    import simtk.unit as unit
    from simtk import openmm
    from simtk.openmm import app
except ImportError:
    import openmm.unit as unit
    from openmm import openmm
    from openmm import app


#=============================================================================================
# Lennard-Jones fluid with an array of sigma values.
#=============================================================================================

class LennardJonesFluidSigma(TestSystem):

    """Create a periodic fluid of Lennard-Jones particles.
    Initial positions are assigned using a subrandom grid to minimize steric interactions.

    Note
    ----
    The default reduced_density is set to 0.05 (gas) so that no minimization is needed to simulate the default system.

    Parameters
    ----------
    nparticles : int, optional, default=1000
        Number of Lennard-Jones particles.
    reduced_density : float, optional, default=0.05
        Reduced density (density * sigma**3); default is appropriate for gas
    mass : openmm.unit.Quantity, optional, default=39.9 * unit.amu
        mass of each particle; default is appropriate for argon
    sigma : openmm.unit.Quantity, optional, default=3.4 * unit.angstrom
        Lennard-Jones sigma parameter; default is appropriate for argon
        This is used for generating the box size.
    sigma_list: list of openmm.unit.Quantity, optional, default: list of sigma (paramater above)
       WIP: Implement box size based on average of this?
       Meant only to change e.g. the FIRST particle parameters, so in a big box, the box size will
       be the same

    epsilon : openmm.unit.Quantity, optional, default=0.238 * unit.kilocalories_per_mole
        Lennard-Jones well depth; default is appropriate for argon
    cutoff : openmm.unit.Quantity, optional, default=None
        Cutoff for nonbonded interactions.  If None, defaults to 3.0 * sigma
    switch_width : openmm.unit.Quantity with units compatible with angstroms, optional, default=3.4 * unit.angstrom
        switching function is turned on at cutoff - switch_width
        If None, no switch will be applied (e.g. hard cutoff).
        Ignored if `shift=True`.
    shift : bool, optional, default=False
        If True, will shift Lennard-Jones potential so energy will be continuous at cutoff (switch_width is ignored).
    dispersion_correction : bool, optional, default=True
        if True, will use analytical dispersion correction (if not using switching function)
    lattice : bool, optional, default=False
        If True, use fcc sphere packing to generate initial positions.  The box
        size will be determined by `nparticles` and `reduced_density`.
    charge : openmm.unit, optional, default=None
        If not None, use alternating plus and minus `charge` for the particle charges.
        Also, if not None, use PME for electrostatics.  Obviously this is no
        longer a traditional LJ system, but this option could be useful for
        testing the effect of charges in small systems.
    ewaldErrorTolerance : float, optional, default=DEFAULT_EWALD_ERROR_TOLERANCE
           The Ewald or PME tolerance.  Used only if charge is not None.

    Examples
    --------

    Create default-size Lennard-Jones fluid.

    >>> fluid = LennardJonesFluid()
    >>> system, positions = fluid.system, fluid.positions

    Create a larger box of Lennard-Jones particles with specified reduced density.

    >>> fluid = LennardJonesFluid(nparticles=1000, reduced_density=0.50)
    >>> system, positions = fluid.system, fluid.positions

    Create Lennard-Jones fluid using switched particle interactions (switched off betwee 7 and 9 A) and more particles.

    >>> fluid = LennardJonesFluid(switch_width=2.0*unit.angstroms, cutoff=9.0*unit.angstroms)
    >>> system, positions = fluid.system, fluid.positions

    Create Lennard-Jones fluid using shifted potential.

    >>> fluid = LennardJonesFluid(cutoff=9.0*unit.angstroms, shift=True)
    >>> system, positions = fluid.system, fluid.positions

    """

    def __init__(self,
                 nparticles=1000,
                 reduced_density=0.05,
                 mass=39.9 * unit.amu,  # argon
                 sigma=3.4 * unit.angstrom,  # argon,
                 sigma_list=None,
                 epsilon=0.238 * unit.kilocalories_per_mole,  # argon,
                 cutoff=None,
                 switch_width=3.4 * unit.angstrom,  # argon
                 shift=False,
                 dispersion_correction=True,
                 lattice=False,
                 charge=None,
                 ewaldErrorTolerance=None,
                 **kwargs):

        if sigma_list is None:
            sigma_list = [sigma for n in range(nparticles)]

        TestSystem.__init__(self, **kwargs)

        # Determine Lennard-Jones cutoff.
        if cutoff is None:
            cutoff = 3.0 * sigma

        if charge is None:  # Charge is zero.
            charge = 0.0 * unit.elementary_charge
            cutoff_type = openmm.NonbondedForce.CutoffPeriodic
        else:
            cutoff_type = openmm.NonbondedForce.PME

        # Create an empty system object.
        system = openmm.System()

        # Determine volume and periodic box vectors.
        number_density = reduced_density / sigma**3
        volume = nparticles * (number_density ** -1)
        box_edge = volume ** (1. / 3.)
        a = unit.Quantity((box_edge,        0 * unit.angstrom, 0 * unit.angstrom))
        b = unit.Quantity((0 * unit.angstrom, box_edge,        0 * unit.angstrom))
        c = unit.Quantity((0 * unit.angstrom, 0 * unit.angstrom, box_edge))
        system.setDefaultPeriodicBoxVectors(a, b, c)

        # Set up periodic nonbonded interactions with a cutoff.
        nb = openmm.NonbondedForce()
        nb.setNonbondedMethod(cutoff_type)
        nb.setCutoffDistance(cutoff)
        nb.setUseDispersionCorrection(dispersion_correction)
        if ewaldErrorTolerance is not None:
            nb.setEwaldErrorTolerance(ewaldErrorTolerance)

        nb.setUseSwitchingFunction(False)
        if (switch_width != None) and (not shift):
            nb.setUseSwitchingFunction(True)
            nb.setSwitchingDistance(cutoff - switch_width)

        for particle_index in range(nparticles):
            system.addParticle(mass)
            if cutoff_type == openmm.NonbondedForce.PME:
                charge_i = charge * ((particle_index % 2) * 2 - 1.)  # Alternate plus and minus
            else:
                charge_i = charge
            nb.addParticle(charge_i, sigma_list[particle_index], epsilon)

        # Add shift if desired.
        if (shift):
            raise NotImplementedError('not implemented for sigma list')

        if lattice:
            box_nm = box_edge / unit.nanometers
            xyz, box = build_lattice(nparticles)
            xyz *= (box_nm / box)
            traj = generate_dummy_trajectory(xyz, box_nm)
            positions = traj.openmm_positions(0)
        else:  # Create initial coordinates using subrandom positions.
            positions = subrandom_particle_positions(nparticles, system.getDefaultPeriodicBoxVectors())
        # Add the nonbonded force.
        system.addForce(nb)

        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('Ar')
        chain = topology.addChain()
        for particle in range(system.getNumParticles()):
            residue = topology.addResidue('Ar', chain)
            topology.addAtom('Ar', element, residue)
        self.topology = topology

        self.system, self.positions = system, positions

    def save(self, outpath, overwrite=False):
        ''' Use Parmed method for saving.
            Append .prmtop to get prmtop
            append .inpcrd to get inpcrd

            And so on.

            This does NOT save the necessary nonbonded methods, switches etc.

            BETTER TO JUST CREATE THE SYSTEM IN THE INPUT SCRIPT
        '''
        top = self.topology
        top.setPeriodicBoxVectors(self.system.getDefaultPeriodicBoxVectors())
        struct = parmed.openmm.load_topology(top, self.system)
        struct.positions = self.positions

        struct.save(outpath, overwrite=overwrite)