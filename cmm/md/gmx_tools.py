import subprocess

def top_as_itp(filepath):
    all_lines = ''
    skip_sections = {'defaults', 'system', 'molecules'}  # Sections to skip
    current_section = None  # Track the current section

    # Open and read the file
    with open(filepath, 'r') as file:
        for line in file:
            # Check for section headers
            if line.startswith('[') and line.endswith(']\n'):
                section_name = line[1:-2].strip()
                if section_name not in skip_sections:
                    current_section = section_name
                    all_lines += line
                else:
                    current_section = None
                continue

            # If current section is None, skip printing lines
            if current_section is None:
                continue

            all_lines += line

    return all_lines

def extract_atomtypes(itp):
    lines = itp.split('\n')
    atomtypes = []
    found_all = False
    for i, line in enumerate(lines):
        if '[ atomtypes ]' in line:
            for atline in lines[i + 1:]:
                if atline.startswith(';'):
                    continue
                if len(atline) < 2:
                    found_all = True
                    break
                atomtypes.append(atline)
        if found_all:
            break

    return atomtypes

def get_resname(itp):
    lines = itp.split('\n')
    for i, line in enumerate(lines):
        if '[ moleculetype ]' in line:
            resname = lines[i+2].strip().split()[0]
    return resname


def replace_resname(itp, old, new):
    new_itp = ''
    lines = itp.split('\n')
    for i, line in enumerate(lines):
        if ' ' + old + ' ' in line:
            line = line.replace(' ' + old + ' ', ' ' + new + ' ')
        elif line.startswith(old):
            line = line.replace(old, new)

        new_itp += line + '\n'
    return(new_itp)


def create_system_top(u_itp, v_itp, u_name, v_name, counterions=None, name='EmptyInside System'):
    # WIP: COUNTERIONS PLZ
    systop = f'''\

#include "{u_name}.itp"
#include "{v_name}.itp"

[ defaults ]
; nbfunc        comb-rule       gen-pairs       fudgeLJ fudgeQQ
1               2               yes             0.5          0.83333333

[ system ]
; Name of the system
{name}

[ atomtypes ]
'''
    for atom_type in extract_atomtypes(u_itp):
        systop += atom_type.strip()  + '\n'
    for atom_type in extract_atomtypes(v_itp):
        systop += atom_type.strip() + '\n'

    systop += '''\

[ molecules ]
; Compound        #mols
XYZ                1
'''
    if counterions:
        systop += counterions + '\n'
    return systop


def run_gmx_commands(v_name, tag, side):
    # Build the commands
    cmd1 = f"gmx editconf -f {v_name}.gro -o {v_name}.gro -c -d 0.1 -nobackup"
    cmd2 = f"gmx editconf -f {tag}.gro -o {tag}.gro -c -box {side / 10} {side / 10} {side / 10} -nobackup"
    cmd3 = f"gmx solvate -cp {tag}.gro -cs {v_name}.gro -o {tag}_{v_name}.gro -p system.top -nobackup"

    # List of commands
    commands = [cmd1, cmd2, cmd3]

    for cmd in commands:
        result = subprocess.run(cmd, shell=True, check=True, text=True,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # Print stdout
        if result.stdout:
            print(result.stdout)

        # Handle stderr separately if needed
        if result.stderr:
            print("GMX says (stderr):", result.stderr)  # Modify this line as needed