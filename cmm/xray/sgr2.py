import os, re, copy
import numpy as np
from scipy.integrate import cumtrapz, trapz
from scipy.optimize import least_squares
from collections import OrderedDict
from .debye import Debye
import warnings
from tqdm.auto import tqdm

class RDF:
    ''' RDF Object.

        r: np.array
            r vector
        g: np.array, same length as r
            g(r) values
        name1: str
            Name of atomtype/element of 'left' atoms
        name2: str
            Name of atomtype/element of 'right' atoms
        region1: str, 'solute' or 'solvent'
            Which region does the 'left' atom belong to
        region2: str, 'solute', or 'solvent'
            Which region does the 'right' atom belong to.

    '''

    def __init__(self, r, g, name1, name2,
                             region1, region2,
                             n1=None, n2=None,
                             damp=None, volume=None,
                             r_max=None, r_avg=None):
        self.r = r
        self.g = g
        self.name1 = name1
        self.name2 = name2
        self.region1 = region1
        self.region2 = region2

        self.n1 = n1  # number of 'left' atoms
        self.n2 = n2  # number of 'right' atoms

        self.diagonal = (name1 == name2) & (region1 == region2)
        self.cross = (region1 != region2)

        self.damp = damp  # Damping object

        self.volume = volume  # Volume used for the N/V RDF normalization
                              # (Volume of your sim. box)

        self.r_max = r_max  # Stop integrating here
        self.r_avg = r_avg  # Use avg. g(r > r_avg) for g0

        self.s = None  # Structure factor. Calculated with SGr. WIP: Move to RDF simply?


    def __str__(self):
        return self.get_info()

    def __repr__(self):
        return self.get_info()

    def get_info(self):
        info = f'{self.name1}-{self.region1}--{self.name2}-{self.region2}: ' +\
               f'RDF with {self.n1} particles in {self.region1} and {self.n2} in {self.region2}. ' +\
               f'Volume: {self.volume}'
        return info


class RDFSet(OrderedDict):
    ''' Container object for a set of RDFs.
        Basically a Dict with a few more options '''

    def __init__(self, *arg, **kwargs):
        self.rdfs = {}
        super(RDFSet, self).__init__(*arg, **kwargs)

    def __len__(self):
        return len(self.keys())

    def __getitem__(self, key):
        if type(key) == slice:
            tmp = RDFSet()
            if key.step is None:
                step = 1
            else:
                step = key.step
            include = list(range(key.start, key.stop, step))
            for i, (rdfkey, rdf) in enumerate(self.items()):
                if i in include:
                    tmp[rdfkey] = rdf
            return tmp
        elif key in self.keys():
            return self.get(key)
        elif type(key) == int:  # use numbered indices.
            tmp = [item for i, item in enumerate(self.items()) if i == key][0]
            real_key, rdf = tmp
            return rdf
        else:
            raise Exception("RDF not in set")

    def get_iq(self, qvec=None, damping=None):
        ''' Calculate Coherent Scattering from set '''

        V = np.unique([rdf.volume for rdf in self.values()])
        assert len(V) == 1, 'RDFs in set have different volumes!'

        left = set([('-'.join(key[:2]), rdf.n1) for (key,rdf) in self.items()])
        right = set([('-'.join(key[2:]), rdf.n2) for (key,rdf) in self.items()])
        if left != right:
            warnings.warn('Asymmetric RDF set: not equal amount of XY and YX pairs')
        total_atom_types = left | right

        if qvec is None:
            sgr = SGr(V[0])
        else:
            sgr = SGr(V[0], qvec=qvec)

        scoh = np.zeros(len(sgr.qvec))
        # XXX  WIP:  Solute/Solvent/Cross
        # 1st term: Atomic
        deb = Debye(qvec=qvec)
        for atm, n in total_atom_types:
            element = atm.split('-')[0]
            scoh += n * deb.atomic_f0(element)**2

        # 2nd term: Interatomic
        for key, rdf in tqdm(self.items()):
            if damping is not None:
                rdf.damp = damping
            scoh += sgr.i_term2(rdf)
        return scoh

    def add_flipped(self, rdf):
        # XXX Needs to be much more robust
        r_rdf = copy.copy(rdf)
        r_rdf.name2 = rdf.name1
        r_rdf.name1 = rdf.name2
        r_rdf.region2 = rdf.region1
        r_rdf.region1 = rdf.region2
        r_rdf.n1 = rdf.n2
        r_rdf.n2 = rdf.n1
        self[(r_rdf.name1, r_rdf.region1,
                   r_rdf.name2, r_rdf.region2)] = r_rdf

    def show(self):
        print(f'|  #  | KEY                   |     STOICHOMETRY     | REGION1, REGION2 | VOLUME ')
        for i, (key, rdf) in enumerate(self.items()):
            key1 = '-'.join(k for k in key[:2])
            key2 = '-'.join(k for k in key[2:])
            keyfmt = f'{key1}--{key2} '
            print(f'| {i:03d} | {keyfmt:22s}| N1: {rdf.n1:5d}, N2: {rdf.n2:5d} | ' +
                  f'{rdf.region1:7s}, {rdf.region2:7s} | {rdf.volume:6.4f} |')



class SGr:
    ''' X-Ray Scattering from pairwise radial distribution functions.

        Can divide signal up in Solute-Solute (u-u), Solute-Solvent (u-v)
        and Solvent-Solvent (v-v) contributions.

        See e.g.: DOI: 10.1088/0953-4075/48/24/244010

    '''

    def __init__(self, volume, qvec=np.arange(0, 10, 0.01)):
        self.qvec = qvec
        self.v = volume

    def structure_factor(self, rdf):
        ''' Calculate the structure factor for a given RDF '''
        r = rdf.r
        g = rdf.g
        dr = r[1] - r[0]

        avg = 1
        if rdf.r_avg is not None:
            avg = np.mean(g[r > rdf.r_avg])

        if rdf.r_max is not None:
            mask = np.zeros(len(r), bool)
            mask[r < rdf.r_max] = True
            r = r[mask]
            g = g[mask]

        qr = self.qvec[:, None] * r[None, :]
        r2sin = 4 * np.pi * dr * (np.sinc(qr / np.pi)) * r[None, :]**2

        if (rdf.region1.lower() == 'solute') and (rdf.region2.lower() == 'solute'):
            g0 = 0
        else:
            g0 = 1

        h = g - g0 * avg
        if rdf.damp is not None:
            h *= rdf.damp.damp(r)

        s = 1 + ((rdf.n2) / self.v) * (r2sin @ h)

        # update the rdf inplace with structure factor
        rdf.s = s

        return s

    def i_term2(self, rdf):
        ''' Coherent X-Ray Scattering, interatomic term'''
        kd = int((rdf.region1 == rdf.region2) and (rdf.name1 == rdf.name2))
        s = self.structure_factor(rdf)

        # Get form factors
        self.atomic_f0(rdf)

        term_2 = (s - 1) * (rdf.n1 - kd) * rdf.f0_1 * rdf.f0_2

        self.term_2 = term_2

        return term_2


    def salacuse_corrected_structure_factor(self, rdf, r_cut):
        ''' 10.1103/PhysRevE.53.2382 '''
        if rdf.s is None:
            _ = self.structure_factor(rdf)
        V = rdf.volume
        q = self.qvec
        g = rdf.g
        r = rdf.r

        # Salacuse 1996, (2):   (not to the "mean" as such but that makes most sense)
        s0 = (1 - np.mean(g[r >= r_cut])) * rdf.n2

        qzeroidx = np.where(q == 0)[0]  # add analytical limit of 1 if q is exactly 0
        sala = (3 / ( q * r_cut)**3) * (np.sin(q * r_cut) - q * r_cut * np.cos(q * r_cut))
        sala[qzeroidx] = 1  # ANALYTICAL LIMIT

        corr = (4 / 3) * np.pi * s0 * r_cut**3 * sala / V

        return rdf.s + corr


    def atomic_f0(self, rdf):
        ''' Calculate atomic form factors and
            update RDF object in place '''
        db = Debye(self.qvec)
        rdf.f0_1 = db.atomic_f0(rdf.name1)
        rdf.f0_2 = db.atomic_f0(rdf.name2)


class VolumeCorrect:
    ''' Class for performing volume corrections to
        RDFs sampled in finite systems.
    '''

    def __init__(self, rdf: RDF, method: str='volume',
                 fit_start: float=40, fit_stop: float=50,
                 p_alpha: float=0.5):
        self.rdf = copy.deepcopy(rdf)
        self.fit_strt = fit_start
        self.fit_stop = fit_stop

        # Perera-correction extra parameters
        self.p_kappa = p_alpha  # Perera alpha parameter in units of Ri

        if method.lower() == 'volume':
            self.correct = self.volume_correct
        elif method.lower() == 'perera':
            self.correct = self.perera_correct
        elif method.lower() == 'vegt':
            self.correct = self.vdv_correct
        else:
            raise RuntimeError('Correction method not understood')

    def volume_correct(self, Ri):
        rdf = copy.deepcopy(self.rdf)
        V = rdf.volume
        N = rdf.n2

        kd = int( (self.rdf.region1 == self.rdf.region2) and
                  (self.rdf.name1 == self.rdf.name2) )

        Vnew = V - (4/3) * Ri**3 *np.pi
        fac = (Vnew/V) * (N / (N - kd))
        return rdf.g * fac

    def perera_correct(self, Ri):
        ''' See 10.1016/j.molliq.2010.05.006 '''
        rdf = copy.deepcopy(self.rdf)
        kappa = 2 * Ri
        alpha = self.p_kappa * Ri
        g0_avg = np.mean(rdf.g[(rdf.r > self.fit_strt) & (rdf.r < self.fit_stop)])
        fac = 1 + ((1 - g0_avg) / 2) * (1 + np.tanh((rdf.r - kappa) / alpha))
        return rdf.g * fac

    def vdv_correct(self, Ri=None):
        ''' See 10.1080/08927022.2017.1416114
            For best explanation. Only implemented for
            g(r)'s sampled to r_max <= 1/2 x boxsize
        '''
        Nb = self.rdf.n2  # Number of b particles in g_ab(r)
        L3 = self.rdf.volume  # Volume of _square_ simulation box.
        r = self.rdf.r
        g = self.rdf.g
        V = (4 / 3) * np.pi * r**3  #  Displaced volume

        kd = int( (self.rdf.region1 == self.rdf.region2) and
                  (self.rdf.name1 == self.rdf.name2) )  * 0  #XXX ONLY works WITHOUT kd!!! WHY?
        rho_b = Nb / L3

        dNab = cumtrapz(4 * np.pi * r**2 * rho_b * (g - 1),
                        dx=(r[1] - r[0]), initial=0)

        fac = (Nb * (1 - V / L3)) / (Nb * (1 - V / L3) - dNab - kd)
        self.fac = fac
        return g * fac


    def fit(self, Ri_guess):
        x0 = np.array([Ri_guess])
        opt = least_squares(self._residual, x0)
        return opt

    def _residual(self, x0):
        g = self.correct(x0[0])
        rdf_fit = RDF(self.rdf.r, g, self.rdf.name1, self.rdf.name2,
                     self.rdf.region1, self.rdf.region2,
                     n1=self.rdf.n1, n2=self.rdf.n2)
        mask = np.zeros(len(rdf_fit.g), bool)
        mask[(rdf_fit.r > self.fit_strt) & (rdf_fit.r < self.fit_stop)] = True
        res = (rdf_fit.g[mask]  - 1)
        return res


## Helper functions
def load_rdfs(rdflist, stoich=None, damp=None):
    ''' Make a list of RDF objects from a list of dat files from e.g. VMD

        The list should contain paths to files in the following format:

            gX_a-Y_b.dat

        where X,Y is the atom type, and a,b is either u or v, for solUte
        and solVent, respectively.

        and the .dat files should contain (r, gr) columns.

        If a stoichometry dict is provided, the n1 and n2
        of the rdf objects will also be filled.

    '''

    rdfs = []
    trslt = {'c': 'cross', 'u':'solute', 'v':'solvent'}

    for rdffile in rdflist:
        name = rdffile.split(os.sep)[-1]
        # use reg exp since atom types can be 1 or 2 chars long:
        atyp1 = re.search('g(.*?)_', name).group(1)
        part1 = re.search(atyp1 + '_(.*?)-', name).group(1)

        atyp2 = re.search('-(.*?)_', name).group(1)
        part2 = re.search('-' +  atyp2 + '_(.*).dat', name).group(1)

        dat = np.genfromtxt(rdffile)
        rdf = RDF(dat[:, 0], dat[:, 1],
                  atyp1, atyp2,
                  trslt[part1], trslt[part2], damp=damp)

        if stoich:
            rdf.n1 = stoich[(atyp1, trslt[part1])]
            rdf.n2 = stoich[(atyp2, trslt[part2])]

        rdfs.append(rdf)

    return rdfs

def rdfset_from_dir(dirname, prmtop, volume=None):
    import MDAnalysis as mda
    import glob

    rdf_files = sorted(glob.glob(dirname + os.sep + '*dat'))

    whered = {'u':'solute', 'v':'solvent', 'c':'cross'}
    # Load universe to get stoichometry.
    u = mda.Universe(prmtop)

    if volume is None:
        # Unfortunately mda cannot get the box size from the prmtop, even though it is there
        with open(prmtop, 'r') as f:
            lines = f.readlines()
        for l, line in enumerate(lines):
            if 'FLAG BOX_DIMENSIONS' in line:
                box = lines[l + 2]
        box = [float(x) for x in box.split()[1:] ]
        V = np.prod(box)
    else:
        V = volume

    set_uc = RDFSet()

    for f, file in enumerate(rdf_files):
        data = np.genfromtxt(file)
        r = data[:, 0]
        g = data[:, 1]

        # find elements from file name. This is very specific to this exact naming scheme...
        file = file.split(os.sep)[-1]
        el1 = re.search('g(.*?)_', file).group(1)
        part1 = re.search(el1 + '_(.*?)-', file).group(1)
        where1 = whered[part1]

        el2 = re.search('-(.*?)_', file).group(1)
        part2 = re.search('-' +  el2 + '_(.*).dat', file).group(1)
        where2 = whered[part2]

        Ni = len([atom for atom in u.atoms if el1 == atom.element])
        Nj = len([atom for atom in u.atoms if el2 == atom.element])

        rdf = RDF(r, g, el1, el2, where1, where2, n1=Ni, n2=Nj)
        rdf.volume = V
        set_uc[(rdf.name1, rdf.region1, rdf.name2, rdf.region2)] = rdf

    return set_uc

