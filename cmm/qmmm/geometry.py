import numpy as np
from ase.geometry import find_mic
from ase.utils.arraywrapper import wrap_array_attribute


class Geometry:

    def __init__(self, atoms, qm_idx, ct_idx, apm=3, midx=0):
        atoms.constraints = ()
        self.atoms = atoms  # Atoms object of entire system.
        self.qm_idx = qm_idx  # indices of (original) QM atoms
        self.ct_idx = ct_idx  # indices of counterions (if any)
        self.apm = apm  # atoms per molecule of the solvent
        self.midx = midx  #  int, Which atom in the MM solvent to base the wrap on.
                          #  E.g. for the ASE TIPnP, use 0, since the sequence
                          # is OHH,OHH,... For the ASE Acetronitrile, maybe use
                          # 1, as the sequence is MeCN,MeCN...

        qm_mask = np.zeros(len(atoms), bool)
        qm_mask[qm_idx] = True
        self.qm_mask = qm_mask

        mm_mask = ~qm_mask
        mm_mask[ct_idx] = False
        self.mm_mask = mm_mask


    def wrap_to_qm_com(self):
        ''' Wrap into MM cell, centering the QM Center of Mass  '''
        atoms = self.atoms.copy()
        # Center QM COM in Cell:
        com = self.atoms[self.qm_mask].get_center_of_mass()
        atoms.translate(-com + atoms.cell.diagonal() / 2)
        # Molecule-wise MIC for solvent
        mmpos = atoms[self.mm_mask].positions.reshape((-1, self.apm, 3))
        outside = mmpos[:, self.midx, :] - atoms.cell.diagonal() / 2
        outside = np.rint(outside /  atoms.cell.diagonal())
        outside = np.repeat(outside, self.apm, axis=0)
        pos = atoms.get_positions()
        pos[self.mm_mask] -= outside * atoms.cell.diagonal()

        # Assuming single-atom counterions!
        if self.ct_idx:
            ctpos = atoms[self.ct_idx].get_positions()
            outside = ctpos - atoms.cell.diagonal() / 2
            outside = np.rint(outside /  atoms.cell.diagonal())
            pos[self.ct_idx] -= outside * atoms.cell.diagonal()
        atoms.set_positions(pos)

        return atoms


    def find_n_closest(self, n, include_ctions=False, include_rest=False):
        ''' Exports an atoms object of the (QM) solute (with qm_index atoms)
            and the closest n solvent molecules.

            include_ctions: bool
                if True, the counterions will also be exported,
                in the sequence [qm, ct, close solvent]

            include_rest: bool
                if True, the rest of the solvent will also be exported,
                but reorganized so you get:
                    [qm (, ct), close solvent, rest of solvent]
        '''
        atoms = self.wrap_to_qm_com()
        com = atoms[self.qm_mask].get_center_of_mass()
        mmpos = atoms[self.mm_mask].positions.reshape((-1, self.apm, 3))

        d = np.linalg.norm(mmpos - com, axis=2)
        idx = np.argpartition(d[:, self.midx], n)
        mask = np.zeros(len(idx), bool)
        mask[idx[:n]] = True
        mask = np.repeat(mask, self.apm)

        return self._export(atoms, mask, include_ctions, include_rest)

    def find_r_closest(self, r, include_ctions=False, include_rest=False):
        atoms = self.wrap_to_qm_com()
        com = self.atoms[self.qm_mask].get_center_of_mass()
        mmpos = atoms[self.mm_mask].positions.reshape((-1, self.apm, 3))

        d = np.linalg.norm(mmpos - com, axis=2)
        mask = d[:, self.midx] < r
        mask = np.repeat(mask, self.apm)

        return self._export(atoms, mask, include_ctions, include_rest)


    def _export(self, atoms, mask, include_ctions, include_rest):
        out_atoms = atoms[self.qm_mask]
        if include_ctions:
            out_atoms += atoms[self.ct_idx]

        out_atoms += atoms[self.mm_mask][mask]
        if include_rest:
            out_atoms += atoms[self.mm_mask][~mask]

        return out_atoms









