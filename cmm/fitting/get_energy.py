import numpy as np
from ase import Atoms
from ase.calculators.combine_mm import CombineMM
from ase.calculators.tip4p import TIP4P, sigma0, epsilon0
from cmm.tools.helpercalcs import DummyCalc
from cmm.fitting.parameters import lj_fe_gs_opls

def get_energy(atoms, mask, charges, mol_size=61, lj=None):
    '''  Get MM (=TIP4P coulomb and OPLS LJ) interaction energy between:
         [Complex + n*Waters] & [Another water] where n can be 0..n. 
         
         Needs to add TIP4P charge sites to waters now included in first
         'monomer'. Rearranges those waters, after getting positions and
         charges, such that the atoms-sequence will be mono1..mono2
         
         atoms:        entire Atoms object
         mask:         bool, True means mono1, so False is the scanning water
         charges:      charges of the complex
         mol_size:     number of atoms in the complex only.
         lj:           Dict of LJ parameters. If None, OPLS will be used.

         WIP: Only has OPLS LJ parameters for C, N, Fe, H. Probably best 
              just to make your own lj dict and stuff it in here so you 
              are sure what you will get.
         
    '''
    mask_mol = np.zeros(len(atoms), bool)
    mask_mol[:mol_size] = True
    mask_wat = np.ones(len(atoms), bool)
    mask_wat[:mol_size] = False

    # we need to add charge sites to the non-scanning waters
    mask_add = mask_wat & mask

    add = atoms[mask_add]
    tip4p = TIP4P()
    pos = tip4p.add_virtual_sites(add.positions)
    chg = tip4p.get_virtual_charges(add)
    new = Atoms('OHHHe' * (len(pos) // 4), positions=pos)

    ### add up new object where the 'MM' part is last, to make it easier
    mono1 = atoms[mask_mol] + new
    mono2 = atoms[~mask]
    final =  mono1 + mono2

    # LJ stuff

    # add 0 LJ on the virtual charge site (He atoms)
    #opls['He'] = (0, 0)
    opls = lj_fe_gs_opls
    if lj:
        opls = lj
    tip4p_lj = {'H': (0, 0),
                'He':(0, 0),
                'O': (epsilon0, sigma0) }

    sigs_mol = np.zeros(len(mono1))
    epss_mol = np.zeros(len(mono1))
    for i, el in enumerate(mono1.get_chemical_symbols()):
        if i > mol_size:
            sigs_mol[i] = tip4p_lj[el][1]
            epss_mol[i] = tip4p_lj[el][0]
        else:
            sigs_mol[i] = opls[el][1]
            epss_mol[i] = opls[el][0]

    # charges 
    new_chg = np.concatenate((charges, chg))

    mol_idx = list(range(len(mono1)))
    final.calc = CombineMM(mol_idx, len(mol_idx), 3,
                           DummyCalc(new_chg),
                           TIP4P(),
                           sigs_mol, epss_mol,
                           np.array([sigma0, 0, 0]),
                           np.array([epsilon0, 0, 0]),
                           rc=np.inf)

    e_tot = final.get_potential_energy()
    e_lj = 0
    e_es = 0
    # CombineMM only saves summed energy. 
    # Add e_lj and e_es to the class to get the
    # two terms:
    if hasattr(final.calc, 'e_lj'):
        e_lj = final.calc.e_lj  
        e_es = final.calc.e_es
    
    return e_tot, e_es, e_lj