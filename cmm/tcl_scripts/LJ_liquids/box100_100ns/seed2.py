import glob 
skip_vv = '''\
       # # v-v takes forever, skip that first time around
       if {{ $ii==1 && $jj==1 }} {{
           continue 
        }}
'''
skip_uv = '''\
       # Only the tuff one now plz
         if {{ $ii<1 || $jj<1 }} {{
             continue 
          }}
'''

which_dict = {'uv':skip_vv, 'vv':skip_uv}

files = glob.glob('/work1/asod/openMM/data/LJFluids/box100_100ns/*/*prod*dcd') 

for key, value in which_dict.items():
    for f in files:
        split = f.split('/')
        fname = split[-1]
        fpath = '/'.join(s for s in split[:-1])

        tag = fname.replace('.dcd', '') + f'_{key}'
        with open(tag + '.tcl', 'w') as tcl:
            tcl.write(f'''\
package require pbctools 2.5

set sels1 [list "index 0" "name Ar"]
set sels2 [list "index 0" "name Ar"]

set len1 [llength $sels1]
set len2 [llength $sels2]

set names1 [list "Ar_u" "Ar_v"]
set names2 [list "Ar_u" "Ar_v"]


proc rdf {{atm1 atm2 outname dir}} {{
    set outfile1 [open $dir/$outname.dat w]

    set sel1 [atomselect top $atm1]
    set sel2 [atomselect top $atm2]

    pbc set {{100.0 100.0 100.0}} -all
    molinfo top set alpha 90
    molinfo top set beta 90
    molinfo top set gamma 90
    set gr0 [measure gofr $sel1 $sel2 delta 0.05 rmax 50.0 usepbc 1 selupdate 0 first 0 last -1 step 1]
    set r [lindex $gr0 0]
    set gr [lindex $gr0 1]
    set igr [lindex $gr0 2]
    set isto [lindex $gr0 3]
    foreach j $r k $gr l $igr m $isto {{
        puts $outfile1 [format "%.14f\\t%.14f\\t%.14f\\t%.14f" $j $k $l $m] 
    }}
    close $outfile1
}}


set dir {fpath}/
set dcd {f}
mol load parm7 /work1/asod/openMM/data/LJFluids/box100/box100.prmtop dcd $dcd

for {{set ii 0}} {{$ii < $len1}} {{incr ii}} {{
    for {{set jj 0}} {{$jj < $len2}} {{incr jj}} {{

       # # Only 1 of each ion:
       if {{ $ii==0 && $jj==0 }} {{
           continue 
        }}
       
       # Ar_v-Ar_u = Ar_u-Ar_v (aka 0, 1)
       if {{ $ii==1 && $jj==0 }} {{
           continue 
        }}

        {value}
       
        set sel1 [lindex $sels1 $ii]
        set sel2 [lindex $sels2 $jj]

        set name {tag}-g[lindex $names1 $ii]-[lindex $names2 $jj]

        puts $name

        rdf $sel1 $sel2 $name $dir
        }}
    }}
mol delete top
        ''')
