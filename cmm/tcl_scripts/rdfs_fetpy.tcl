package require pbctools 2.5

# element selector doesnt work on HPC vmd for some odd reason
#set sels1 [list "element Fe" "element C" "element H and residue < 2" "element N" "element Cl" "element O" "element H and residue > 2"]
set sels1 [list "name FE" "name \"C.*\" and residue < 2" "name \"H.*\" and residue < 2" "name \"N.*\" and residue < 2" "residue 3 to 4" "name O" "name \"H.*\" and residue > 2"]
set sels2 $sels1 

set len1 [llength $sels1]
set len2 [llength $sels2]

set names1 [list "Fe_u" "C_u" "H_u" "N_u" "Cl_u" "O_v" "H_v"]
set names2 $names1

### Functions used
proc dirlist { ext } {
    set contents [glob -type d $ext]
    foreach item $contents {
       append out $item
       append out "\n"
       }
    return $out
    }

proc flist { dir ext } {
    set contents [glob -directory $dir $ext]
    foreach item $contents {
       append out $item
       append out "\n"
       }
    return $out
    }


proc rdf {atm1 atm2 outname dir} {
    set outfile1 [open $dir/$outname.dat w]

    set sel1 [atomselect top $atm1]
    set sel2 [atomselect top $atm2]

    molinfo top set alpha 90
    molinfo top set beta 90
    molinfo top set gamma 90
    set gr0 [measure gofr $sel1 $sel2 delta 0.01 rmax 27.1476535 usepbc 1 selupdate 0 first 0 last -1 step 1]
    set r [lindex $gr0 0]
    set gr [lindex $gr0 1]
    set igr [lindex $gr0 2]
    set isto [lindex $gr0 3]
    foreach j $r k $gr l $igr m $isto {
        puts $outfile1 [format "%.14f\t%.14f\t%.14f\t%.14f" $j $k $l $m]
    }
    close $outfile1
}


#set dirs [dirlist  ../../../data/FeTPY/fetpy*]
#set dirs [dirlist  ../../../data/FeTPY/fetpy*ljfit*]
#set dirs [dirlist  ../../../data/FeTPY/fetpy*rminquestion*]
#set dirs [dirlist  ../../../data/FeTPY/fetpy*fancy*4*]
#set dirs [dirlist /work1/asod/openMM/data/FeTPY/*long_hot*]
#set dirs [dirlist /work1/asod/openMM/data/FeTPY/*da*]
#
#set dirs [dirlist /work1/asod/openMM/data/FeTPY/*nlls*]
set dirs [dirlist /work1/asod/openMM/data/FeTPY/*2021*]

foreach dir $dirs {
    puts $dir

    #set dcds [flist $dir fetpy*nvt*.dcd]
    set dcds [flist $dir fetpy*2021*.dcd]
    set dcd [lindex $dcds 0];  # there should only be one file per dir

    mol load parm7 ../../data/md/fetpy/FETPY_solv.prmtop dcd $dcd

    for {set ii 0} {$ii < $len1} {incr ii} {
        for {set jj 0} {$jj < $len2} {incr jj} {

            # Only 1 silver:
            if { $ii==0 && $jj==0 } {
                continue 
             }

            set sel1 [lindex $sels1 $ii]
            set sel2 [lindex $sels2 $jj]

            set name g[lindex $names1 $ii]-[lindex $names2 $jj]

            puts $name

            rdf $sel1 $sel2 $name $dir
            }
        }
    mol delete top
}


